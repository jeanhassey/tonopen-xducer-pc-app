﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using tonopen_xducer_pc_app.Model;

namespace tonopen_xducer_pc_app
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			FileParser fp = new FileParser();
			//fp.Parse("C:\\P033\\Data8\\01616-E9.DTA");
			fp.Parse("C:\\Users\\jdillon\\Desktop\\01589-E2.DTA");

			FileParser fp1 = new FileParser();
			fp1.Parse("C:\\Users\\jdillon\\Desktop\\04919-L9.DTA");// 01471-D9.DTA");

			//FinalTestDataP048 finalTestDataP048 = new FinalTestDataP048(fp1.UnitData[0]);

			FileParser fp2 = new FileParser();
			fp2.Parse("C:\\Users\\jdillon\\Desktop\\04119-E.DTA");

			FileParser fp3 = new FileParser();
			fp3.Parse("C:\\Users\\jdillon\\Desktop\\04019-F9.DTA");

			GageFileParser gfp = new GageFileParser();
			

			//var result = FileLineParser.ParseLine("FH    1/11/19   01616-E9 P033     11       12      ");
			//result = FileLineParser.ParseLine("ID    15:01:21  ###      B452     B453     B454     B456     B457     B458     B459     B460     B461     B462     B463     B465    ");
			//result = FileLineParser.ParseLine("BH    10/2/18   5.0      Daycomp ");
			//result = FileLineParser.ParseLine("R1    16:48:19  0.0000    349.205  321.990  343.570  344.910  344.622  342.768  343.607  351.632  354.866  355.194  352.641  354.939  355.549  356.287  319.057  324.096  326.746  332.381  337.482  350.129  369.383  333.802  322.895  331.344  331.866  331.716  344.541  345.910");
			//result = FileLineParser.ParseLine("S14   06:17:27  0.0000     0.2222  -0.0034  -0.1820  -0.1406   0.0538   0.2998  -0.1861   0.2536   0.0722   0.5078   0.2529   0.4571");
			//result = FileLineParser.ParseLine("ADC   10:40:49  25             41       41       43       42       43       38       43       38       38       39       38       44");
			//result = FileLineParser.ParseLine("RO    06:23:32  0.0000    469.569  468.667  490.514  479.487  485.637  480.956  485.407  488.546  454.265  462.009  461.375  460.432");
			//result = FileLineParser.ParseLine("V4    06:15:44  0.0000     0.2233   0.0386  -0.1412  -0.0952   0.1079   0.3338  -0.1436   0.2881   0.1227   0.5532   0.3110   0.5017");
			//result = FileLineParser.ParseLine("BH    1/12/19   24.0     WarmUp   3.300   ");

			//DatabaseRow dbr = new DatabaseRow(fp.UnitData[0]);
			XducerDatabaseClass db = new XducerDatabaseClass();

			if (db.OpenXducerDatabase())
			{
				//if (db.AddFileToDB(fp.UnitData))
				//{

				//}
				//if (db.AddFileToDB(fp1.UnitData))
				//{

				//}
				//if (db.AddFileToDB(fp2.UnitData))
				//{

				//}
				if (db.AddFileToDB(fp3.UnitData))
				{

				}
				//if (db.AddFileToDB(gfp.Parse("C:\\Users\\jdillon\\Desktop\\DeltaLog2.log")))
				//{

				//}

				db.CloseXducerDatabase();
			}
		}
	}
}
