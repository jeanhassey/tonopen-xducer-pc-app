﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tonopen_xducer_pc_app.Model
{
	public enum LineType
	{
		LINE_TYPE_BLOCKHEADER,
		LINE_TYPE_FILEHEADER,
		LINE_TYPE_ID,
		LINE_TYPE_ADC,
		LINE_TYPE_SPANTEST,
		LINE_TYPE_V4, //UpDnTest, Temptest
		LINE_TYPE_V5, //Precal
		LINE_TYPE_N1,
		LINE_TYPE_N2,
		LINE_TYPE_N3,
		LINE_TYPE_RO,
		LINE_TYPE_RI,
		LINE_TYPE_R1,
		LINE_TYPE_R2,
		LINE_TYPE_R3,
		LINE_TYPE_R4,
		LINE_TYPE_SEC,
		LINE_TYPE_UNK,
	}

	public static class FileLineParser
	{
		const int LINE_ID_IDX = 0;
		const int LINE_DATETIME_IDX = 1;
		const int LINE_WO_IDX = 2;
		const int LINE_STARTDATA_IDX = 3;
		const int LINE_TESTTYPE_IDX = 3;
		const int LINE_MODELTYPE_IDX = 3;
		const int LINE_STARTCHNL_IDX = 4;
		const int LINE_NUMUNITS_IDX = 5;

		public static TestFileLine ParseLine(string Data)
		{
			string[] separatingStrings = { " ", "\t" };
			string[] data = Data.Split(separatingStrings, StringSplitOptions.RemoveEmptyEntries);
			LineType lineType = LineType.LINE_TYPE_UNK;

			TestFileLine line;
			switch (data[LINE_ID_IDX])
			{
				case "FH":
					line = new FileHeaderLine(DateTime.Parse(data[LINE_DATETIME_IDX]), 
						data[LINE_WO_IDX], 
						GetModelType(data[LINE_MODELTYPE_IDX]), 
						Convert.ToInt32(data[LINE_STARTCHNL_IDX]), 
						Convert.ToInt32(data[LINE_NUMUNITS_IDX]));
					break;

				case "ID":
					List<string> IDs = new List<string>();
					for (int i = LINE_STARTDATA_IDX; i < data.Length; i++)
					{
						IDs.Add(data[i]);
					}

					line = new IdentifierLine(DateTime.Parse(data[LINE_DATETIME_IDX]), IDs);
					break;

				case "BH":
					BlockHeaderLine bhl = new BlockHeaderLine(DateTime.Parse(data[LINE_DATETIME_IDX]), Convert.ToDouble(data[2]), GetTestType(data[LINE_TESTTYPE_IDX]));
					if (data.Length > 4)
						bhl.TestParameter2 = Convert.ToDouble(data[4]);
					line = bhl;
					break;

				default:
					switch (data[LINE_ID_IDX])
					{
						case "SP":
							lineType = LineType.LINE_TYPE_SPANTEST;
							break;
						case "R1":
							lineType = LineType.LINE_TYPE_R1;
							break;
						case "R2":
							lineType = LineType.LINE_TYPE_R2;
							break;
						case "R3":
							lineType = LineType.LINE_TYPE_R3;
							break;
						case "R4":
							lineType = LineType.LINE_TYPE_R4;
							break;
						case "V4":
							lineType = LineType.LINE_TYPE_V4;
							break;
						case "V5":
							lineType = LineType.LINE_TYPE_V5;
							break;
						case "N1":
							lineType = LineType.LINE_TYPE_N1;
							break;
						case "N2":
							lineType = LineType.LINE_TYPE_N2;
							break;
						case "N3":
							lineType = LineType.LINE_TYPE_N3;
							break;
						case "RO":
							lineType = LineType.LINE_TYPE_RO;
							break;
						case "RI":
							lineType = LineType.LINE_TYPE_RI;
							break;
						case "ADC":
							lineType = LineType.LINE_TYPE_ADC;
							break;
						default:
							if (data[LINE_ID_IDX].StartsWith("S") &&  int.TryParse(data[LINE_ID_IDX].Substring(1), out int result))
								lineType = LineType.LINE_TYPE_SEC;
							break;
					}

					List<double> lineData = new List<double>();
					for (int i = LINE_STARTDATA_IDX; i < data.Length; i++)
					{
						lineData.Add(Convert.ToDouble(data[i]));
					}
					line = new DataLine(lineType, DateTime.Parse(data[LINE_DATETIME_IDX]), Convert.ToDouble(data[2]), lineData);
					break;
			}

			return line;
		}

		public static ModelType GetModelType(string model)
		{
			if (model == "P033")
			{
				return ModelType.MODEL_TYPE_XL;
			}
			else if (model == "P048")
			{
				return ModelType.MODEL_TYPE_AVIA;
			}
			else
				return ModelType.MODEL_TYPE_UNK;
		}

		private static BlockType GetTestType(string test)
		{
			BlockType bt = BlockType.BLOCK_TYPE_UNK;
			switch(test)
			{
				case "SpanTest":
					bt = BlockType.BLOCK_TYPE_SPAN;
					break;
				case "UpDnTest":
					bt = BlockType.BLOCK_TYPE_UPDOWN;
					break;
				case "UnitU/D":
					bt = BlockType.BLOCK_TYPE_UPDOWNUNIT;
					break;
				case "Rin/Rout":
					bt = BlockType.BLOCK_TYPE_RINROUT;
					break;
				case "WarmUp":
					bt = BlockType.BLOCK_TYPE_WARMUP;
					break;
				case "IOPTest":
					bt = BlockType.BLOCK_TYPE_IOP;
					break;
				case "Temptest":
					bt = BlockType.BLOCK_TYPE_TEMP;
					break;
				case "Daycomp":
					bt = BlockType.BLOCK_TYPE_TEMPCOMP;
					break;
				case "PreCal":
					bt = BlockType.BLOCK_TYPE_PRECAL;
					break;
			}
			return bt;
		}
	}

	public class TestFileLine
	{
		public TestFileLine(LineType lt, DateTime dt)
		{
			LineType = lt;
			DateTime = dt;

		}
		public LineType LineType { get; set; }
		public DateTime DateTime { get; set; }

	}

	public class FileHeaderLine : TestFileLine
	{
		public FileHeaderLine(DateTime dt, string wo, ModelType m, int s, int n) : base(LineType.LINE_TYPE_FILEHEADER, dt)
		{
			WorkOrder = wo;
			Model = m;
			StartChannel = s;
			NumUnits = n;
		}

		public string WorkOrder { get; set; }
		public ModelType Model { get; set; }
		public int StartChannel { get; set; }
		public int NumUnits { get; set; }
	}

	public class IdentifierLine : TestFileLine
	{
		public IdentifierLine(DateTime dt, List<string> u) : base(LineType.LINE_TYPE_ID, dt)
		{
			Units = u;
		}

		public List<string> Units { get; set; }
	}

	public class BlockHeaderLine : TestFileLine
	{
		public BlockHeaderLine(DateTime dt, double t, BlockType b) : base(LineType.LINE_TYPE_BLOCKHEADER, dt)
		{
			TestParameter1 = t;
			BlockType = b;
		}

		public double TestParameter1 { get; set; }
		public double TestParameter2 { get; set; }
		public BlockType BlockType { get; set; }
	}

	public class DataLine : TestFileLine
	{
		public DataLine(LineType lt, DateTime dt, double tp, List<double> d) : base(lt, dt)
		{
			TestParameter = tp;
			Data = d;
		}

		public double TestParameter { get; set; }
		public List<double> Data { get; set; }
	}

}
