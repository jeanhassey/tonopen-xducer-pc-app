﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tonopen_xducer_pc_app.Model
{
	public enum ModelType
	{
		MODEL_TYPE_UNK,
		MODEL_TYPE_AVIA,
		MODEL_TYPE_XL
	}

	public enum TableType
	{
		TABLE_MAIN,
		TABLE_PRECAL
	}

	public enum BlockType
	{
		BLOCK_TYPE_FILEHEADER,
		BLOCK_TYPE_HEADER,
		BLOCK_TYPE_ID,
		BLOCK_TYPE_IOP,
		BLOCK_TYPE_PRECAL,
		BLOCK_TYPE_RINROUT,
		BLOCK_TYPE_SPAN,
		BLOCK_TYPE_TEMP,
		BLOCK_TYPE_TEMPCOMP,
		BLOCK_TYPE_UPDOWN,
		BLOCK_TYPE_UPDOWNUNIT,
		BLOCK_TYPE_WARMUP,
		BLOCK_TYPE_UNK,
	}

	public class UnitData
	{
		public UnitData(DateTime dt, FileType ft, string u, string w, ModelType m, int c)
		{
			DateTime = dt;
			FileType = ft;
			UnitID = u;
			WorkOrder = w;
			Model = m;
			Channel = c;

			SpanTest = new List<SpanTest>();
			UpDownTest = new List<UpDownTest>();
			UpDownUnit = new List<UpDownTest>();
			TempTest = new List<TempTest>();
			WarmUpTest = new List<WarmUpTest>();
			ResInOut = new List<ResInOut>();
			IOPTest = new List<IOPTest>();
			PrecalTest = new List<PrecalTest>();
			TempCompTest = new List<TempCompTest>();
		}

		public DateTime DateTime { get; set; }
		public string UnitID { get; set; }
		public string WorkOrder { get; set; }
		public ModelType Model { get; set; }
		public FileType FileType { get; set; }
		public int Channel { get; set; }

		//public FileHeader FileHeader { get; set; }
		//public Identifier Identifier { get; set; }
		public List<SpanTest> SpanTest { get; set; }
		public List<UpDownTest> UpDownTest { get; set; }
		public List<UpDownTest> UpDownUnit { get; set; }
		public List<TempTest> TempTest { get; set; }
		public List<WarmUpTest> WarmUpTest { get; set; }
		public List<ResInOut> ResInOut { get; set; }
		public List<IOPTest> IOPTest { get; set; }
		public List<PrecalTest> PrecalTest { get; set; }
		public List<TempCompTest> TempCompTest { get; set; }
	}

	public class GageData
	{
		public string WorkOrder { get; set; }
		public ModelType Model { get; set; }
		public string UnitID { get; set; }
		public string Initials { get; set; }
		public DateTime Date { get; set; }
		public int Unbond1 { get; set; }
		public int Unbond2 { get; set; }
		public int Unbond3 { get; set; }
		public int Unbond4 { get; set; }
		public int Bond1 { get; set; }
		public int Bond2 { get; set; }
		public int Bond3 { get; set; }
		public int Bond4 { get; set; }
	}

	public class FileTestBlock
	{
		public FileTestBlock(DateTime d, BlockType b)
		{
			BlockType = b;
			Date = d;
		}
		public BlockType BlockType { get; set; }
		public DateTime Date { get; set; }
	}

	public class FileHeader : FileTestBlock
	{
		public FileHeader(DateTime d) : base(d, BlockType.BLOCK_TYPE_FILEHEADER)
		{
			BlockType = BlockType.BLOCK_TYPE_FILEHEADER;
		}

		public FileHeader(DateTime date, string wo, ModelType m, int sc, int nu) : base(date, BlockType.BLOCK_TYPE_HEADER)
		{
			//BlockType = BlockType.BLOCK_TYPE_HEADER;
			//Date = date;
			WorkOrder = wo;
			Model = m;
			StartChannel = sc;
			NumUnits = nu;
		}

		public DateTime Date { get; set; }
		public string WorkOrder { get; set; }
		public ModelType Model { get; set; }
		public int StartChannel { get; set; }
		public int NumUnits { get; set; }
	}

	public class Identifier : FileTestBlock
	{
		public Identifier(DateTime time, string id) : base (time, BlockType.BLOCK_TYPE_ID)
		{
			Time = time;
			UnitID = id;
		}
		public DateTime Time { get; set; }
		public string UnitID { get; set; }
	}

	public class TestBlockHeader : FileTestBlock
	{
		public TestBlockHeader(DateTime dt, BlockType bt, double d1, double d2) : base (dt,bt)
		{
			Date = dt;
			FollowingBlockType = bt;
			Data1 = d1;
			Data2 = d2;
		}
		public DateTime Date { get; set; }
		public BlockType FollowingBlockType { get; set; }
		public double Data1 { get; set; }
		public double Data2 { get; set; }
	}

	public class SpanTest : FileTestBlock
	{
		public SpanTest(DateTime d, double t, double e) : base(d, BlockType.BLOCK_TYPE_SPAN)//, DateTime dt0, double d0, DateTime dt3, double d3)
		{
			Temperature = t;
			Excitation = e;
		}
		public double Excitation { get; set; }
		public double Temperature { get; set; }
		public DateTime DateTime0g { get; set; }
		public double Data0g { get; set; }
		public DateTime DateTime3g { get; set; }
		public double Data3g { get; set; }
	}

	public class UpDownTest : FileTestBlock // And Unit U/D (List size = 3 whereas UpDownTest size = 6)
	{
		public UpDownTest(DateTime dt, double t, double e) : base(dt, BlockType.BLOCK_TYPE_UPDOWN) 
		{
			Temperature = t;
			Excitation = e;
			UpDownData = new List<UpDownEntry>();
		}

		public double Excitation { get; set; }
		public double Temperature { get; set; }
		public double AvgDelta { get; set; } //LowUpDn or HiUpDn
		public double AvgUp { get; set; } //HiTemp or LowTemp
		public double MidUpDn { get; set; } //avg 2 25C readings
		public double MidTemp { get; set; } //avg 2 25C readings

		//printed if UD fails P033
		public double UpDownPercent { get; set; }

		public List<UpDownEntry> UpDownData { get; set; }

		public class UpDownEntry
		{
			public UpDownEntry(DateTime dt, double d)
			{
				DateTime = dt;
				Data = d;
			}
			public DateTime DateTime { get; set; }
			public double Data { get; set; }
		}
	}

	public class PrecalTest : FileTestBlock
	{
		public PrecalTest(DateTime dt, double t, double u) : base(dt, BlockType.BLOCK_TYPE_PRECAL) 
		{
			Temperature = t;
			UnkParameter = u;
			PrecalData = new List<PrecalEntry>();
		}

		public double Temperature { get; set; }
		public double UnkParameter { get; set; }

		public List<PrecalEntry> PrecalData { get; set; }

		public class PrecalEntry
		{
			public PrecalEntry(DateTime dt, double d)
			{
				DateTime = dt;
				Data = d;
			}
			public DateTime DateTime { get; set; }
			public double Data { get; set; }
		}
	}

	public class TempTest : FileTestBlock
	{
		public TempTest(DateTime dt, double t, double e) : base(dt, BlockType.BLOCK_TYPE_TEMP) 
		{
			Temperature = t;
			Excitation = e;
		}

		public DateTime DateTime { get; set; }
		public double Temperature { get; set; }
		public double Excitation { get; set; }
		public double Data { get; set; }
		public double AvgDelta { get; set; } //LowUpDn or HiUpDn
		public double AvgUp { get; set; } //HiTemp or LowTemp
	}

	public class TempCompTest : FileTestBlock
	{
		public TempCompTest(DateTime dt, double t) : base(dt, BlockType.BLOCK_TYPE_TEMPCOMP)
		{
			Temperature = t;
			Data = new List<TempCompEntry>();
		}

		public double Temperature { get; set; }
		public List<TempCompEntry> Data { get; set; }

		public class TempCompEntry
		{
			public TempCompEntry(DateTime dt, int r, double d)
			{
				DateTime = dt;
				RVal = r;
				Data = d;
			}
			public int RVal { get; set; }
			public DateTime DateTime { get; set; }
			public double Data { get; set; }
		}
	}

	public class WarmUpTest : FileTestBlock
	{
		public WarmUpTest(DateTime dt, double t, double e) : base(dt, BlockType.BLOCK_TYPE_WARMUP) 
		{
			Temperature = t;
			Excitation = e;
			WarmUpData = new List<WarmUpEntry>();
		}

		public double Excitation { get; set; }
		public double Temperature { get; set; }

		public double DeltaE { get; set; }

		public List<WarmUpEntry> WarmUpData { get; set; }

		public class WarmUpEntry
		{
			public WarmUpEntry(DateTime dt, int s, double d)
			{
				DateTime = dt;
				Second = s;
				Data = d;
			}
			public int Second { get; set; }
			public DateTime DateTime { get; set; }
			public double Data { get; set; }
		}
	}
	public class ResInOut : FileTestBlock
	{
		public ResInOut(DateTime dt, double t) : base(dt, BlockType.BLOCK_TYPE_RINROUT) 
		{
			Temperature = t;
		}

		public double Temperature { get; set; }
		public DateTime DateTimeOut { get; set; }
		public double DataOut { get; set; }
		public DateTime DateTimeIn { get; set; }
		public double DataIn { get; set; }
	}

	public class IOPTest : FileTestBlock
	{
		public IOPTest(DateTime dt, int p) : base(dt, BlockType.BLOCK_TYPE_IOP) 
		{
			Pressure = p;
			IOPData = new List<IOPEntry>();
		}

		public int Pressure { get; set; }
		
		public List<IOPEntry> IOPData { get; set; }

		public class IOPEntry
		{
			public IOPEntry(DateTime dt, int p, int d)
			{
				DateTime = dt;
				Pressure = p;
				Data = d;
			}

			public DateTime DateTime { get; set; }
			public int Pressure { get; set; }
			public int Data { get; set; }
		}
	}
}
