﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tonopen_xducer_pc_app.Model
{
	class GageFileParser
	{
		public List<GageData> GageData { get; set; }
		public List<GageData> Parse(string FilePath)
		{
			if (FilePath.Contains("DeltaLog2.log"))
			{
				GageData = new List<GageData>();
				//Read the contents of the file into a stream
				using (StreamReader reader = new StreamReader(FilePath))
				{
					while (!reader.EndOfStream)
					{
						string[] separatingStrings = { ",","\t","\n","\r","\\", "\""};
						string dataLine = reader.ReadLine();
						string[] data = dataLine.Split(separatingStrings, StringSplitOptions.RemoveEmptyEntries);

						GageData gageData = new GageData()
						{
							WorkOrder = data[0],
							Model = FileLineParser.GetModelType(data[1]),
							UnitID = data[2],
							Initials = data[3],
							Date = DateTime.Parse(data[4]),
							Unbond1 = Convert.ToInt32(data[5]),
							Unbond2 = Convert.ToInt32(data[6]),
							Unbond3 = Convert.ToInt32(data[7]),
							Unbond4 = Convert.ToInt32(data[8]),
							Bond1 = Convert.ToInt32(data[9]),
							Bond2 = Convert.ToInt32(data[10]),
							Bond3 = Convert.ToInt32(data[11]),
							Bond4 = Convert.ToInt32(data[12])
						};

						GageData.Add(gageData);
					}

					return GageData;
				}
			}
			return null;
		}
	}
}
