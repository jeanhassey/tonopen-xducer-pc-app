﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tonopen_xducer_pc_app.Model
{
	public class FinalTestDataP033
	{
		public FinalTestDataP033(UnitData unitData)
		{
			Calculate(unitData);
		}


		private List<TempTest> LastTests;

		public string UnitID { get; set; }
		public string WorkOrder { get; set; }
		public DateTime Date { get; set; }
		public double Excitation { get; set; }
		public double ForceTest0g { get; set; }
		public double ForceTest3g { get; set; }
		public double Span { get; set; }
		//public double UpDownDelta { get; set; }
		public double TempTestLo { get; set; }
		public double TempTestMid { get; set; }
		public double TempTestHi { get; set; }
		public double WarmUp19 { get; set; }
		public double WarmUp20 { get; set; }
		public double WarmUpDeltaE { get; set; }
		public double InputResistance { get; set; }
		public double OutputResistance { get; set; }
		public double IOP25 { get; set; }
		public double IOP42 { get; set; }
		public double IOP77 { get; set; }

		


		public double MidUpDn { get; set; } //avg 2 25C readings
		public double MidTemp { get; set; } //avg 2 25C readings


		// See VBP033Print.bas
		private void Calculate(UnitData unitData)
		{
			// jmd assume one span test
			SpanTest spanTest = unitData.SpanTest[0];
			List<TempTest> tempTestBlocks = unitData.TempTest;

			ForceTest0g = spanTest.Data0g;
			ForceTest3g = spanTest.Data3g;
			Span = spanTest.Data3g - spanTest.Data0g;

			int count = tempTestBlocks.Count;
			if (count >= 4)
			{
				// Get 4 last TempTest blocks in file and use those
				LastTests = new List<TempTest>();
				LastTests.Add(tempTestBlocks[count - 1]);
				LastTests.Add(tempTestBlocks[count - 2]);
				LastTests.Add(tempTestBlocks[count - 3]);
				LastTests.Add(tempTestBlocks[count - 4]);

				if (unitData.UpDownTest.Count == 1)
				{
					for (int i = 0; i < 4; i++)
					{
						ComputeAverages(unitData.UpDownTest[0]);
					}

					//for (int i = 0; i < 4; i++)
					//{
					//	if (LastTests[i].Temperature == 25)
					//	{
					//		MidUpDn = MidUpDn + LastTests[i].AvgDelta / 2.0;
					//		MidTemp = MidTemp + LastTests[i].AvgUp / 2.0;
					//	}
					//}

					//UpDownDelta = MidUpDn / Span * 100.0;
				}
			}

			for (int i = 0; i < 4; i++)
			{
				if (LastTests[i].Temperature == 13)
				{
					TempTestLo = LastTests[i].Data;
				}
				else if (LastTests[i].Temperature == 24)
				{
					if(TempTestMid == 0.0)
						TempTestMid = LastTests[i].Data;
					else
						TempTestMid = (TempTestMid + LastTests[i].Data)/ 2.0;
				}
				else if (LastTests[i].Temperature == 35)
				{
					TempTestHi = LastTests[i].Data;
				}
			}

			// jmd assume only one warmup test
			WarmUp19 = unitData.WarmUpTest[0].WarmUpData[18].Data;
			WarmUp20 = unitData.WarmUpTest[0].WarmUpData[19].Data;

			WarmUpDeltaE = WarmUp20 - WarmUp19;
			// jmd assume only one res test
			InputResistance = unitData.ResInOut[0].DataIn;
			OutputResistance = unitData.ResInOut[0].DataOut;
			// jmd assume only one res test
			// jmd assume pressure in this order
			IOP25 = unitData.IOPTest[0].IOPData[0].Data;
			IOP42 = unitData.IOPTest[0].IOPData[1].Data;
			IOP77 = unitData.IOPTest[0].IOPData[2].Data;
		}

		// See VBP033Print.bas
		public void ComputeAverages(UpDownTest upDownTest)
		{
			double[] delta = new double[4];

			delta[0] = (upDownTest.UpDownData[0].Data + upDownTest.UpDownData[2].Data) / 2 - upDownTest.UpDownData[1].Data; // up with low/mid
			delta[1] = -(upDownTest.UpDownData[1].Data + upDownTest.UpDownData[3].Data) / 2 + upDownTest.UpDownData[2].Data; // down with low/mid
			delta[2] = (upDownTest.UpDownData[2].Data + upDownTest.UpDownData[4].Data) / 2 - upDownTest.UpDownData[3].Data; // up with mid/hi
			delta[3] = -(upDownTest.UpDownData[3].Data + upDownTest.UpDownData[5].Data) / 2 + upDownTest.UpDownData[4].Data; // down with mid/hi

			double Sum = delta.Sum();
			double Max = delta.Max();
			double Min = delta.Min();

			upDownTest.AvgDelta = (Sum - Max - Min) / 2; // 'throw away high and low and ave. middle 2
			
			if(Span > 0.0)
				upDownTest.UpDownPercent = upDownTest.AvgDelta / Span;
		}
	}
}
