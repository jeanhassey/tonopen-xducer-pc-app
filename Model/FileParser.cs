﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tonopen_xducer_pc_app.Model
{
	public enum FileType
	{
		FILE_TYPE_UNK,
		FILE_TYPE_PRECAL,
		FILE_TYPE_TEMPCOMP,
		FILE_TYPE_FINAL
	}
	public class FileParser
	{
		public List<UnitData> UnitData { get; set; }

		public int NumUnits { get; set; }
		public DateTime FileDate { get; set; }
		public string WorkOrder { get; set; }
		ModelType ModelType { get; set; }
		FileType FileType { get; set; }
		int StartChannel { get; set; }

		private BlockType CurrentBlockType { get; set; }
		private int CurrentBlockLine { get; set; }

		public enum FileParseErrors
		{
			FILE_ERR_NONE,
			FILE_ERR_NUM_UNITS,
			FILE_ERR_UNK_LINE,
			FILE_ERR_HDR_LINE_MISMATCH,
			FILE_ERR_TOO_MANY_LINES,
		}

		public FileParseErrors Parse(string FilePath)
		{
			if (FilePath.Contains("9.DTA"))
			{
				FileType = FileType.FILE_TYPE_FINAL;
			}
			else if (FilePath.Contains("2.DTA"))
			{
				FileType = FileType.FILE_TYPE_TEMPCOMP;
			}
			else if (FilePath.Contains(".DTA"))
			{
				FileType = FileType.FILE_TYPE_PRECAL;
			}
			else
				FileType = FileType.FILE_TYPE_UNK;

			//Read the contents of the file into a stream
			using (StreamReader reader = new StreamReader(FilePath))
			{
				while (!reader.EndOfStream)
				{	
					var line = FileLineParser.ParseLine(reader.ReadLine());

					if (line.LineType == LineType.LINE_TYPE_FILEHEADER)
					{
						FileHeaderLine fhl = (FileHeaderLine)line;
						NumUnits = fhl.NumUnits;
						FileDate = fhl.DateTime;
						ModelType = fhl.Model;
						WorkOrder = fhl.WorkOrder;//.Substring(0,7);
						StartChannel = fhl.StartChannel;
					}
					else if (line.LineType == LineType.LINE_TYPE_ID)
					{
						IdentifierLine idl = (IdentifierLine)line;

						if (idl.Units.Count == NumUnits)
						{
							int channel = StartChannel;
							UnitData = new List<UnitData>();

							foreach (string unitId in idl.Units)
							{
								// The file header line has the date and the ID line has the time.
								UnitData.Add(new Model.UnitData(GetComboDateTime(FileDate, idl.DateTime), FileType, unitId, WorkOrder, ModelType, channel));

								//jmd Assuming incrementing channel number follows the order of the units ids in the file
								channel++;
							}
						}
						else
							return FileParseErrors.FILE_ERR_NUM_UNITS; //error in file - num unit ids doesn't equal number present
					}
					else if (line.LineType == LineType.LINE_TYPE_BLOCKHEADER)
					{
						BlockHeaderLine bhl = (BlockHeaderLine)line;
						CurrentBlockType = bhl.BlockType;
						CurrentBlockLine = 1;

						switch (CurrentBlockType)
						{
							case BlockType.BLOCK_TYPE_IOP:
								foreach (UnitData unit in UnitData)
								{
									unit.IOPTest.Add(new IOPTest(bhl.DateTime, Convert.ToInt32(bhl.TestParameter1)));
								}
								break;
							case BlockType.BLOCK_TYPE_PRECAL:
								foreach (UnitData unit in UnitData)
								{
									unit.PrecalTest.Add(new PrecalTest(bhl.DateTime, bhl.TestParameter1, bhl.TestParameter2));
								}
								break;
							case BlockType.BLOCK_TYPE_RINROUT:
								foreach (UnitData unit in UnitData)
								{
									unit.ResInOut.Add(new ResInOut(bhl.DateTime, bhl.TestParameter1));
								}
								break;
							case BlockType.BLOCK_TYPE_SPAN:
								foreach (UnitData unit in UnitData)
								{
									unit.SpanTest.Add(new SpanTest(bhl.DateTime, bhl.TestParameter1, bhl.TestParameter2));
								}
								break;
							case BlockType.BLOCK_TYPE_TEMP:
								foreach (UnitData unit in UnitData)
								{
									unit.TempTest.Add(new TempTest(bhl.DateTime, bhl.TestParameter1, bhl.TestParameter2));
								}
								break;
							case BlockType.BLOCK_TYPE_TEMPCOMP:
								foreach (UnitData unit in UnitData)
								{
									unit.TempCompTest.Add(new TempCompTest(bhl.DateTime, Convert.ToDouble(bhl.TestParameter1)));
								}
								break;
							case BlockType.BLOCK_TYPE_UPDOWN:
								foreach (UnitData unit in UnitData)
								{
									unit.UpDownTest.Add(new UpDownTest(bhl.DateTime, bhl.TestParameter1, bhl.TestParameter2));
								}
								break;
							case BlockType.BLOCK_TYPE_UPDOWNUNIT:
								foreach (UnitData unit in UnitData)
								{
									unit.UpDownUnit.Add(new UpDownTest(bhl.DateTime, bhl.TestParameter1, bhl.TestParameter2));
								}
								break;
							case BlockType.BLOCK_TYPE_WARMUP:
								foreach (UnitData unit in UnitData)
								{
									unit.WarmUpTest.Add(new WarmUpTest(bhl.DateTime, bhl.TestParameter1, bhl.TestParameter2));
								}
								break;
						}
					}
					else if (line.LineType == LineType.LINE_TYPE_UNK)
						return FileParseErrors.FILE_ERR_UNK_LINE;
					else
					{
						DataLine dl = (DataLine)line;

						// Test block
						switch (line.LineType)
						{
							case LineType.LINE_TYPE_ADC:
								if (CurrentBlockType == BlockType.BLOCK_TYPE_IOP)
								{
									ParseIOPTest(dl);
								}
								else
									return FileParseErrors.FILE_ERR_HDR_LINE_MISMATCH; //error wrong header for this line
								CurrentBlockLine++;
								break;
							case LineType.LINE_TYPE_N1:
							case LineType.LINE_TYPE_N2:
							case LineType.LINE_TYPE_N3:
								if (CurrentBlockType == BlockType.BLOCK_TYPE_UPDOWNUNIT)
								{
									ParseUDUnitTest(dl);
								}
								else
									return FileParseErrors.FILE_ERR_HDR_LINE_MISMATCH; //error wrong header for this line
								CurrentBlockLine++;
								break;
							case LineType.LINE_TYPE_R1:
							case LineType.LINE_TYPE_R2:
							case LineType.LINE_TYPE_R3:
							case LineType.LINE_TYPE_R4:
								if (CurrentBlockType == BlockType.BLOCK_TYPE_TEMPCOMP)
								{
									ParseTempCompTest(dl);
								}
								else
									return FileParseErrors.FILE_ERR_HDR_LINE_MISMATCH; //error wrong header for this line
								CurrentBlockLine++;
								break;
							case LineType.LINE_TYPE_RI:
							case LineType.LINE_TYPE_RO:
								if (CurrentBlockType == BlockType.BLOCK_TYPE_RINROUT)
								{
									ParseRinRoutTest(dl);
								}
								else
									return FileParseErrors.FILE_ERR_HDR_LINE_MISMATCH; //error wrong header for this line
								CurrentBlockLine++;
								break;
							case LineType.LINE_TYPE_SEC:
								if (CurrentBlockType == BlockType.BLOCK_TYPE_WARMUP)
								{
									ParseWarmUpTest(dl);
								}
								else
									return FileParseErrors.FILE_ERR_HDR_LINE_MISMATCH; //error wrong header for this line
								CurrentBlockLine++;
								break;

							case LineType.LINE_TYPE_SPANTEST:
								if (CurrentBlockType == BlockType.BLOCK_TYPE_SPAN)
								{
									ParseSpanTest(dl);
								}
								else
									return FileParseErrors.FILE_ERR_HDR_LINE_MISMATCH; //error wrong header for this line
								CurrentBlockLine++;
								break;

							case LineType.LINE_TYPE_UNK:
								break;

							case LineType.LINE_TYPE_V4:
								if (CurrentBlockType == BlockType.BLOCK_TYPE_UPDOWN)
								{
									ParseUDTest(dl);
								}
								else if (CurrentBlockType == BlockType.BLOCK_TYPE_TEMP)
								{
									ParseTempTest(dl);
								}
								else if (CurrentBlockType == BlockType.BLOCK_TYPE_PRECAL)
								{
									ParsePrecalTest(dl);
								}
								else
									return FileParseErrors.FILE_ERR_HDR_LINE_MISMATCH; //error wrong header for this line
								CurrentBlockLine++;
								break;

							case LineType.LINE_TYPE_V5:
								if (CurrentBlockType == BlockType.BLOCK_TYPE_PRECAL)
								{
									ParsePrecalTest(dl);
								}
								else
									return FileParseErrors.FILE_ERR_HDR_LINE_MISMATCH; //error wrong header for this line
								CurrentBlockLine++;
								break;
						}
					}
				}
			}

			return FileParseErrors.FILE_ERR_NONE;
		}

		private DateTime GetComboDateTime(DateTime date, DateTime time)
		{
			return new DateTime(date.Year,			
				date.Month,			
				date.Day,
				time.Hour,
				time.Minute,
				time.Second,
				time.Millisecond);
		}

		private void ParsePrecalTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.PrecalTest.Count > 0)
				{
					var test = unit.PrecalTest[unit.PrecalTest.Count - 1];

					if (CurrentBlockLine > 0 && CurrentBlockLine <= 6)
					{
						test.PrecalData.Add(new PrecalTest.PrecalEntry(GetComboDateTime(test.Date, dataLine.DateTime), 
							dataLine.Data[unit.Channel - StartChannel]));
					}
					else
					{
						// error should only be 6 lines
					}
				}
				else
				{
					//error - ud test should be initialized
				}
			}
		}

		private void ParseTempCompTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.TempCompTest.Count > 0)
				{
					var test = unit.TempCompTest[unit.TempCompTest.Count - 1];

					if (CurrentBlockLine > 0 && CurrentBlockLine <= 4)
					{
						test.Data.Add(new TempCompTest.TempCompEntry(GetComboDateTime(test.Date, dataLine.DateTime), 
							CurrentBlockLine, 
							dataLine.Data[unit.Channel - StartChannel]));
					}
					else
					{
						// error should only be 6 lines
					}
				}
				else
				{
					//error - ud test should be initialized
				}
			}
		}

		private void ParseIOPTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.IOPTest.Count > 0)
				{
					var test = unit.IOPTest[unit.IOPTest.Count - 1];

					if (CurrentBlockLine > 0 && CurrentBlockLine <= 3)
					{
						test.IOPData.Add(new IOPTest.IOPEntry(GetComboDateTime(test.Date, dataLine.DateTime), 
							Convert.ToInt32(dataLine.TestParameter), 
							Convert.ToInt32(dataLine.Data[unit.Channel - StartChannel])));
					}
					else
					{
						// error should only be 6 lines
					}
				}
				else
				{
					//error - ud test should be initialized
				}
			}
		}

		private void ParseRinRoutTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.ResInOut.Count > 0)
				{
					var test = unit.ResInOut[unit.ResInOut.Count - 1];

					DateTime dt = GetComboDateTime(test.Date, dataLine.DateTime);

					if (CurrentBlockLine == 1)
					{
						test.DateTimeOut = dt;
						test.DataOut = dataLine.Data[unit.Channel - StartChannel];
					}
					else if (CurrentBlockLine == 2)
					{
						test.DateTimeIn = dt;
						test.DataIn = dataLine.Data[unit.Channel - StartChannel];
					}
					else
					{
						// error should only be 2 lines
					}
				}
				
				else
				{
					//error - span test should be initialized
				}
			}
		}

		private void ParseUDTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.UpDownTest.Count > 0)
				{
					var test = unit.UpDownTest[unit.UpDownTest.Count - 1];

					if(CurrentBlockLine > 0 && CurrentBlockLine <= 6)
					{
						test.UpDownData.Add(new UpDownTest.UpDownEntry(GetComboDateTime(test.Date, dataLine.DateTime), 
							dataLine.Data[unit.Channel - StartChannel]));
					}
					else
					{
						// error should only be 6 lines
					}
				}
				else
				{
					//error - ud test should be initialized
				}
			}
		}

		private void ParseUDUnitTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.UpDownUnit.Count > 0)
				{
					var test = unit.UpDownUnit[unit.UpDownUnit.Count - 1];

					if (CurrentBlockLine > 0 && CurrentBlockLine <= 3)
					{
						test.UpDownData.Add(new UpDownTest.UpDownEntry(GetComboDateTime(test.Date, dataLine.DateTime), 
							dataLine.Data[unit.Channel - StartChannel]));
					}
					else
					{
						// error should only be 3 lines
					}
				}
				else
				{
					//error - ud test should be initialized
				}
			}
		}

		private void ParseWarmUpTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.WarmUpTest.Count > 0)
				{
					var test = unit.WarmUpTest[unit.WarmUpTest.Count - 1];

					if (CurrentBlockLine > 0 && CurrentBlockLine <= 20)
					{
						test.WarmUpData.Add(new WarmUpTest.WarmUpEntry(GetComboDateTime(test.Date, dataLine.DateTime), 
							CurrentBlockLine, 
							dataLine.Data[unit.Channel - StartChannel]));

						if(test.WarmUpData.Count == 20)
						{
							test.DeltaE = test.WarmUpData[19].Data - test.WarmUpData[18].Data;
						}
					}
					else
					{
						// error should be 20 lines
					}
				}
				else
				{
					//error - ud test should be initialized
				}
			}
		}

		private void ParseTempTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.TempTest.Count > 0)
				{
					var test = unit.TempTest[unit.TempTest.Count - 1];

					if (CurrentBlockLine == 1)
					{
						test.DateTime = GetComboDateTime(test.Date, dataLine.DateTime);
						test.Data = dataLine.Data[unit.Channel - StartChannel];
					}
					else
					{
						// error should only be 1 line
					}
				}
				else
				{
					//error - ud test should be initialized
				}
			}
		}

		private void ParseSpanTest(DataLine dataLine)
		{
			foreach (UnitData unit in UnitData)
			{
				if (unit.SpanTest.Count > 0)
				{
					var test = unit.SpanTest[unit.SpanTest.Count - 1];

					if (CurrentBlockLine == 1)
					{
						test.DateTime0g = GetComboDateTime(test.Date, dataLine.DateTime);

						test.Data0g = dataLine.Data[unit.Channel - StartChannel];
					}
					else if (CurrentBlockLine == 2)
					{
						test.DateTime3g = GetComboDateTime(test.Date, dataLine.DateTime);

						test.Data3g = dataLine.Data[unit.Channel - StartChannel];
					}
					else
					{
						// error should only be two lines
					}
				}
				else
				{
					//error - span test should be initialized
				}
			}
		}
	}
}
