﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace tonopen_xducer_pc_app.Model
{
	class XducerDatabaseClass : DatabaseClass
	{
		public enum XducerDatabaseState
		{
			XDUCER_DB_STATE_CLOSED = 0,
			XDUCER_DB_STATE_OPEN_FAIL = 1,
			XDUCER_DB_STATE_OPEN = 2,
			XDUCER_DB_STATE_NEW_ENTRY = 3,
			XDUCER_DB_STATE_ENTRY_EXISTS = 4,
		}

		public enum PrecalTableFields
		{
			PRECAL_ID,	//auto increment
			PRECAL_UNIT_ID,
			PRECAL_MODEL,
			PRECAL_DATETIME,
			PRECAL_WORKORDER,
			PRECAL_TC_DATETIME,
			PRECAL_TC_TEMP,
			PRECAL_TC_R1,
			PRECAL_TC_R2,
			PRECAL_TC_R3,
			PRECAL_TC_R4,
			PRECAL_PC1_DATETIME,
			PRECAL_PC1_TEMP,
			PRECAL_PC1_VAL,
			PRECAL_PC2_DATETIME,
			PRECAL_PC2_TEMP,
			PRECAL_PC2_VAL,
			PRECAL_PC3_DATETIME,
			PRECAL_PC3_TEMP,
			PRECAL_PC3_VAL
		}

		public static string[] PRECAL_FIELD_NAMES =
		{
			"precal_id",
			"unit_id",
			"model_type",
			"record_datetime",
			"work_order",
			"tempcomp_datetime",
			"tempcomp_temp",
			"tempcomp_r1",
			"tempcomp_r2",
			"tempcomp_r3",
			"tempcomp_r4",
			"precal_datetime1",
			"precal_temp1",
			"precal_val1",
			"precal_datetime2",
			"precal_temp2",
			"precal_val2",
			"precal_datetime3",
			"precal_temp3",
			"precal_val3",
		};

		public enum XducerDatabaseFields
		{
			DB_UNIT_ID = 0,		// XXXX
			DB_REC_DATETIME,        // When this entry created in DB
			DB_MODEL_TYPE,
			DB_FILE_SRC,
			DB_ENTRY_TYPE,		// Imagining original entry or additional
			DB_WORKORDER,
			DB_PEAK_FREQ_HZ,	// 
			DB_WIDTH_HZ,		//
			//DB_INIT_R_DATETIME,     //
			//DB_INIT_R1,         // Initial convoluted bonded resistance measurements 
			//DB_INIT_R2,         //
			//DB_INIT_R3,         //
			//DB_INIT_R4,         //
			//DB_BEFORE_T_DATETIME,
			//DB_BEFORE_T_TEMP,
			//DB_BEFORE_T_VOLTS,
			//DB_AFTER_T_DATETIME,
			//DB_AFTER_T_TEMP,
			//DB_AFTER_T_VOLTS,
			//DB_AFTER_BI_DATETIME,
			//DB_AFTER_BI_TEMP,
			//DB_AFTER_BI_VOLTS,
			DB_GAGE_DATE,		// Gager data
			DB_GAGE_ID,			// Gager's initials
			DB_BOND1,
			DB_BOND2,
			DB_BOND3,
			DB_BOND4,
			DB_UNBOND1,
			DB_UNBOND2,
			DB_UNBOND3,
			DB_UNBOND4,			// End gager data
			//DB_UD_DATETIME,
			//DB_UDUNIT_DT1,
			//DB_UDUNIT_TEMP1,
			//DB_UDUNIT_DATA1,
			//DB_UDUNIT_DT2,
			//DB_UDUNIT_TEMP2,
			//DB_UDUNIT_DATA2,
			//DB_UDUNIT_DT3,
			//DB_UDUNIT_TEMP3,
			//DB_UDUNIT_DATA3,
			DB_REAL_R_1,
			DB_REAL_R_2,
			DB_REAL_R_3,
			DB_REAL_R_4,
			DB_REAL_R_5,
			DB_REAL_R_6,
			DB_UD_SPAN_TGT,
			DB_FORCE_DATETIME,
			DB_FORCE_EXCIT,
			DB_FORCE_TEMP,
			DB_FORCE_0G,
			DB_FORCE_3G,
			DB_FORCE_SPAN,
			//DB_FINAL_DATETIME,
			//DB_FINAL_EXCITATION,
			//DB_FINAL_TEMP,
			//DB_FINAL_FORCE_0G,
			//DB_FINAL_FORCE_3G,
			//DB_FINAL_UD_DATETIME1,
			//DB_FINAL_UD_TEMP_1, // Double-check: I need a temp for all 6 of these?
			//DB_FINAL_UD_1,
			//DB_FINAL_UD_DATETIME2,
			//DB_FINAL_UD_TEMP_2,
			//DB_FINAL_UD_2,
			//DB_FINAL_UD_DATETIME3,
			//DB_FINAL_UD_TEMP_3,
			//DB_FINAL_UD_3,
			//DB_FINAL_UD_DATETIME4,
			//DB_FINAL_UD_TEMP_4,
			//DB_FINAL_UD_4,
			//DB_FINAL_UD_DATETIME5,
			//DB_FINAL_UD_TEMP_5,
			//DB_FINAL_UD_5,
			//DB_FINAL_UD_DATETIME6,
			//DB_FINAL_UD_TEMP_6,
			//DB_FINAL_UD_6,
			DB_WARMUP_DATETIME,
			DB_WARMUP_TEMP,
			DB_WARMUP_EXCITATION,
			DB_WARMUP_1S,
			DB_WARMUP_2S,
			DB_WARMUP_3S,
			DB_WARMUP_4S,
			DB_WARMUP_5S,
			DB_WARMUP_6S,
			DB_WARMUP_7S,
			DB_WARMUP_8S,
			DB_WARMUP_9S,
			DB_WARMUP_10S,
			DB_WARMUP_11S,
			DB_WARMUP_12S,
			DB_WARMUP_13S,
			DB_WARMUP_14S,
			DB_WARMUP_15S,
			DB_WARMUP_16S,
			DB_WARMUP_17S,
			DB_WARMUP_18S,
			DB_WARMUP_19S,
			DB_WARMUP_20S,
			DB_WARMUP_DELTAE,
			DB_RORI_TEMP,
			DB_RORI_DATETIMEOUT,
			DB_RORI_RO,
			DB_RORI_DATETIMEIN,
			DB_RORI_RI,
			DB_IOP_PRESS,
			DB_IOP_DATETIME1,
			DB_IOP_PRESS1,
			DB_IOP_DATA1,
			DB_IOP_DATETIME2,
			DB_IOP_PRESS2,
			DB_IOP_DATA2,
			DB_IOP_DATETIME3,
			DB_IOP_PRESS3,
			DB_IOP_DATA3,
			DB_AVG_UP_LO_TEMP,
			DB_AVG_UP_MED_TEMP,
			DB_AVG_UP_HI_TEMP,
			DB_UD_DELTA_LO_TEMP,
			DB_UD_DELTA_MED_TEMP,
			DB_UD_DELTA_HI_TEMP,
			DB_TEMPCOMP_DATETIME,
			DB_TEMPCOMP_TEMP,
			DB_TEMPCOMP_R1,
			DB_TEMPCOMP_R2,
			DB_TEMPCOMP_R3,
			DB_TEMPCOMP_R4,
			DB_TEMPTEST_DATETIME,
			DB_TEMPTEST_TEMP,
			DB_TEMPTEST_EXC,
			DB_TEMPTEST_DATA,
			//DB_PRECAL_TEMP,
			//DB_PRECAL_UNK,
			//DB_PRECAL_DT1,
			//DB_PRECAL_DATA1,
			//DB_PRECAL_DT2,
			//DB_PRECAL_DATA2,
			//DB_PRECAL_DT3,
			//DB_PRECAL_DATA3,
			//DB_PRECAL_DT4,
			//DB_PRECAL_DATA4,
			//DB_PRECAL_DT5,
			//DB_PRECAL_DATA5,
			//DB_PRECAL_DT6,
			//DB_PRECAL_DATA6,
			DB_NUM_FIELDS
		}

		public static string[] DB_FIELD_NAMES =
			{ 
			"unit_id",
			"record_datetime", //MySQL retrieves and displays DATETIME values in ' YYYY-MM-DD hh:mm:ss ' format. 
			//mysql> INSERT INTO ts (col) VALUES ('2020-01-01 10:10:10'),
    //->     ('2020-01-01 10:10:10+05:30'), ('2020-01-01 10:10:10-08:00');
			"model_type",
			"file_source_type",
			"entry_type",
			"work_order",
			"peak_freq_hz",
			"width_hz",
			//"init_r_datetime",
			//"init_r1",
			//"init_r2",
			//"init_r3",
			//"init_r4",
			//"before_t_datetime",
			//"before_t_temp",
			//"before_t_volts",
			//"after_t_datetime",
			//"after_t_temp",
			//"after_t_volts",
			//"after_bi_datetime",
			//"after_bi_temp",
			//"after_bi_volts",
			"gage_date",
			"gager_id",
			"bond_1",
			"bond_2",
			"bond_3",
			"bond_4",
			"unbond_1",
			"unbond_2",
			"unbond_3",
			"unbond_4",
			//"ud_datetime",
			//"udunit_datetime1",
			//"udunit_temp1",
			//"udunit_data1",
			//"udunit_datetime2",
			//"udunit_temp2",
			//"udunit_data2",
			//"udunit_datetime3",
			//"udunit_temp3",
			//"udunit_data3",
			"real_r_val1",
			"real_r_val2",
			"real_r_val3",
			"real_r_val4",
			"real_r_val5",
			"real_r_val6",
			"ud_span_target",
			"force_datetime", // from *.DTA file
			"force_excitation", // from *.DTA file
			"force_temp", // from *.DTA file
			"force_test_0g", // from *.DTA file
			"force_test_3g", // from *.DTA file
			"force_test_span",
			//"final_datetime",
			//"Final_excitation",
			//"final_temp",
			//"final_force_0g",
			//"final_force_3g",
			//"final_ud_datetime1",
			//"final_ud_temp1",
			//"final_ud_1",
			//"final_ud_datetime2",
			//"final_ud_temp2",
			//"final_ud_2",
			//"final_ud_datetime3",
			//"final_ud_temp3",
			//"final_ud_3",
			//"final_ud_datetime4",
			//"final_ud_temp4",
			//"final_ud_4",
			//"final_ud_datetime5",
			//"final_ud_temp5",
			//"final_ud_5",
			//"final_ud_datetime6",
			//"final_ud_temp6",
			//"final_ud_6",
			"warmup_datetime",
			"warmup_excitation",
			"warmup_1sec",
			"warmup_2sec",
			"warmup_3sec",
			"warmup_4sec",
			"warmup_5sec",
			"warmup_6sec",
			"warmup_7sec",
			"warmup_8sec",
			"warmup_9sec",
			"warmup_10sec",
			"warmup_11sec",
			"warmup_12sec",
			"warmup_13sec",
			"warmup_14sec",
			"warmup_15sec",
			"warmup_16sec",
			"warmup_17sec",
			"warmup_18sec",
			"warmup_19sec", // from final 9.DTA file
			"warmup_20sec", // from final 9.DTA file
			"warmup_delta_e",
			"resistance_io_temp", // from *.DTA file
			"resistance_io_datetimeout", // from *.DTA file
			"resistance_io_out", // from *.DTA file
			"resistance_io_datetimein", // from *.DTA file
			"resistance_io_in", // from *.DTA file
			"iop_pressure",
			"iop_datetime1",
			"iop_press1",
			"iop_data1",
			"iop_datetime2",
			"iop_press2",
			"iop_data2",
			"iop_datetime3",
			"iop_press3",
			"iop_data3",
			"avg_up_lo_temp",
			"avg_up_med_temp",
			"avg_up_hi_temp",
			"ud_delta_lo_temp",
			"ud_delta_med_temp",
			"ud_delta_hi_temp",
			"tempcomp_datetime",
			"tempcomp_temp",
			"tempcomp_r1",
			"tempcomp_r2",
			"tempcomp_r3",
			"tempcomp_r4",
			"temptest_datetime",
			"temptest_temp",
			"temptest_excitation",
			"temptest_data",
			//"precal_temp",
			//"precal_unk",
			//"precal_datetime1",
			//"precal_data1",
			//"precal_datetime2",
			//"precal_data2",
			//"precal_datetime3",
			//"precal_data3",
			//"precal_datetime4",
			//"precal_data4",
			//"precal_datetime5",
			//"precal_data5",
			//"precal_datetime6",
			//"precal_data6",
			""
		};

		private const string DB_NAME = "MfgEng";
		private const string DB_SERVER = "10.19.1.30";//"mfgsql";
		private const string DB_USERNAME = "mfgeng";
		private const string DB_PASSWORD = DB_USERNAME;
		private readonly string KEY1_FIELD_NAME = "unit_id";
		private readonly string KEY2_FIELD_NAME = "work_order";

		private const string XDUCER_TABLE_NAME = "tonopen_transducers";
		private const string PRECAL_TABLE_NAME = "tonopen_xducer_precal";
		private const string UPDOWN_TABLE_NAME = "tonopen_xducer_updown";

		public const string DB_TEST_PASS = "PASS";
		public const string DB_TEST_FAIL = "FAIL";

		//public string KEY1_FIELD_NAME1 => KEY1_FIELD_NAME;

		//public string KEY2_FIELD_NAME1 => KEY2_FIELD_NAME;

		public XducerDatabaseClass()
		{
			KEY1_FIELD_NAME = DB_FIELD_NAMES[(int)XducerDatabaseFields.DB_UNIT_ID];
			KEY2_FIELD_NAME = DB_FIELD_NAMES[(int)XducerDatabaseFields.DB_WORKORDER];
		}

		public bool OpenXducerDatabase()
		{
			return (base.OpenDatabase(DB_SERVER, DB_NAME, DB_USERNAME, DB_PASSWORD));
		}

		public void CloseXducerDatabase()
		{
			base.CloseDatabase();
		}

		//public bool CreateNewEntry(string unitId)
		//{
		//	return (base.CreateNewDBEntry(unitId, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), KEY1_FIELD_NAME, KEY2_FIELD_NAME, XDUCER_TABLE_NAME));
		//}

		public bool AddFileToDB(List<GageData> gageData)
		{
			for (int i = 0; i < gageData.Count; i++)
			{
				if (RecordExists(gageData[i].UnitID, gageData[i].WorkOrder, KEY1_FIELD_NAME, KEY2_FIELD_NAME, XDUCER_TABLE_NAME))
				{
					// This data will go on the initial data line
					// jmd make sure it goes on inital entry line. This will update all lines with these unit ids/work orders
					AddToInitalEntry(DatabaseRow.CreateTableEntry(DatabaseEntryType.DB_ENTRY_GAGE, gageData[i]), XDUCER_TABLE_NAME);
				}
				else
				{
					CreateNewEntry(DatabaseRow.CreateTableEntry(DatabaseEntryType.DB_ENTRY_GAGE, gageData[i]), XDUCER_TABLE_NAME);
				}
			}
			// check if unit/WO is already there, if not create new line?
			return true;
		}

		public bool AddFileToDB(List<UnitData> unitData)
		{
			// check if unit/WO is already there, if not create new line?

			int numUnits = unitData.Count;

			for(int unitIdx = 0; unitIdx < numUnits; unitIdx++)
			{
				if (!RecordExists(unitData[unitIdx].UnitID, unitData[unitIdx].WorkOrder, KEY1_FIELD_NAME, KEY2_FIELD_NAME, PRECAL_TABLE_NAME))
				{
					CreateNewEntry(DatabaseRow.CreateTableEntry(DatabaseEntryType.DB_ENTRY_INIT, unitData[unitIdx], 0), XDUCER_TABLE_NAME);
				}

				if (unitData[unitIdx].FileType == FileType.FILE_TYPE_PRECAL &&
					unitData[unitIdx].PrecalTest.Count == 3 && 
					unitData[unitIdx].TempCompTest.Count == 1)
				{
					if (!RecordExists(unitData[unitIdx].UnitID, unitData[unitIdx].WorkOrder, KEY1_FIELD_NAME, KEY2_FIELD_NAME, PRECAL_TABLE_NAME))
					{
						CreateNewEntry(DatabaseRow.CreateTableEntry(DatabaseEntryType.DB_ENTRY_PRECAL, unitData[unitIdx], 0), PRECAL_TABLE_NAME);
					}
				}

				int numTests = unitData[unitIdx].TempCompTest.Count;
				if (numTests > 0 && unitData[unitIdx].FileType != FileType.FILE_TYPE_PRECAL)
				{
					for (int testIdx = 1; testIdx < numTests; testIdx++)
					{
						CreateNewEntry(DatabaseRow.CreateTableEntry(DatabaseEntryType.DB_ENTRY_TEMPCOMP, unitData[unitIdx], testIdx), XDUCER_TABLE_NAME);
					}
				}

				numTests = unitData[unitIdx].TempTest.Count;
				if (numTests > 0)
				{
					for (int testIdx = 1; testIdx < numTests; testIdx++)
					{
						CreateNewEntry(DatabaseRow.CreateTableEntry(DatabaseEntryType.DB_ENTRY_TEMPTEST, unitData[unitIdx], testIdx), XDUCER_TABLE_NAME);
					}
				}

				numTests = unitData[unitIdx].UpDownTest.Count;
				if (numTests > 0)
				{
					for (int testIdx = 0; testIdx < numTests; testIdx++)
					{
						CreateNewEntry(DatabaseRow.CreateTableEntry(DatabaseEntryType.DB_ENTRY_UD, unitData[unitIdx], testIdx), UPDOWN_TABLE_NAME);
					}
				}

				numTests = unitData[unitIdx].UpDownUnit.Count;
				if (numTests > 0)
				{
					for (int testIdx = 0; testIdx < numTests; testIdx++)
					{
						CreateNewEntry(DatabaseRow.CreateTableEntry(DatabaseEntryType.DB_ENTRY_UD_UNIT, unitData[unitIdx], testIdx), UPDOWN_TABLE_NAME);
					}
				}
			}

			return true;
		}

		public bool AddToInitalEntry(List<DatabaseCell> RowDefinition, string tableName)
		{
			if (RowDefinition == null)
				return false;

			bool success = true;

			// Creates list of fields to populate
			string CommandString = "UPDATE " + tableName + " Set ";
			//string CommandStringEnd = " WHERE " + KEY1_FIELD_NAME + " = '" + sKey1 + "' AND " + KEY2_FIELD_NAME + " = '" + sKey2 + "'";

			for (int i = 0; i < RowDefinition.Count; i++)
			{
				if (RowDefinition[i].Data != null && 
					RowDefinition[i].ColumnName != KEY1_FIELD_NAME && 
					RowDefinition[i].ColumnName != KEY2_FIELD_NAME &&
					RowDefinition[i].ColumnName != "entry_type") // Null fields not included in UPDATE statement
				{
					CommandString += RowDefinition[i].ColumnName;
					CommandString += " = '" + RowDefinition[i].DataString + "', ";
				}
			}

			// remove trailing comma
			CommandString = CommandString.Substring(0, CommandString.Length - 2);

			// Create list of values that correspond with the fields
			CommandString += " WHERE ";

			for (int i = 0; i < RowDefinition.Count; i++)
			{
				if (RowDefinition[i].ColumnName == KEY1_FIELD_NAME ||
					RowDefinition[i].ColumnName == KEY2_FIELD_NAME)
				{
					if (RowDefinition[i].Data != null)  // Null fields not included in INSERT statement
					{
						CommandString += RowDefinition[i].ColumnName;
						CommandString += " = '" + RowDefinition[i].DataString;
						CommandString += "' AND ";
					}
				}
			}

			// remove trailing AND
			CommandString = CommandString.Substring(0, CommandString.Length - 5);

			// Make sure new data added to inital entry
			CommandString += " AND entry_type = '" + Convert.ToInt32(DatabaseEntryType.DB_ENTRY_INIT).ToString() + "'";


			MySqlCommand MySqlCommand = new MySqlCommand(CommandString, MySqlConnection);

			try
			{
				MySqlCommand.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				string res = ex.ToString();
				success = false;
			}
			finally
			{
				MySqlCommand.Dispose();
			}

			return success;
		}

		public bool CreateNewEntry(List<DatabaseCell> RowDefinition, string tableName)
		{
			if (RowDefinition == null)
				return false;

			bool success = true;

			// Creates list of fields to populate
			string CommandString = "INSERT INTO " + tableName + "(";

			for (int i = 0; i < RowDefinition.Count; i++)
			{
				if (RowDefinition[i].Data != null) // Null fields not included in INSERT statement
				{
					CommandString += RowDefinition[i].ColumnName;
					CommandString += ", ";
				}
			}

			// remove trailing comma
			CommandString = CommandString.Substring(0, CommandString.Length - 2);

			// Create list of values that correspond with the fields
			CommandString += ") VALUES ('";

			for (int i = 0; i < RowDefinition.Count; i++)
			{
				if (RowDefinition[i].Data != null)  // Null fields not included in INSERT statement
				{
					CommandString += RowDefinition[i].DataString;
					CommandString += "', '";
				}
			}

			// remove trailing comma
			CommandString = CommandString.Substring(0, CommandString.Length - 4);

			CommandString += "')";


			MySqlCommand MySqlCommand = new MySqlCommand(CommandString, MySqlConnection);

			try
			{
				MySqlCommand.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				string res = ex.ToString();
				success = false;
			}
			finally
			{
				MySqlCommand.Dispose();
			}

			return success;
		}

		//public bool CreateNewEntry(UnitData data)
		//{
		//	//INSERT INTO Customer(FirstName, LastName, City, Country, Phone)
		//	//VALUES('Craig', 'Smith', 'New York', 'USA', 1 - 01 - 993 2800)
		//	bool success = true;

		//	string CommandString = "INSERT INTO " + XDUCER_TABLE_NAME + "(";// + KEY1_FIELD_NAME + ", " + KEY2_FIELD_NAME + "," + "entry_type" + ") VALUES ('" + data.UnitID + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "', '" + "0" + "')";

		//	for(int i = 0;i < 3/*DB_FIELD_NAMES.Length-1*/;i++)
		//	{
		//		CommandString += DB_FIELD_NAMES[i];
		//		CommandString += ", ";
		//	}

		//	CommandString += DB_FIELD_NAMES[3];
		//	CommandString += ") VALUES ('";
		//	CommandString += data.UnitID;
		//	CommandString += "', '";
		//	CommandString += DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
		//	CommandString += "', '";
		//	CommandString += "0"; //entrytype
		//	CommandString += "', '";
		//	CommandString += data.WorkOrder;
		//	CommandString += "')";
		//	//data.WorkOrder
		//	//data.Channel
		//	//data.Model

		//	MySqlCommand MySqlCommand = new MySqlCommand(CommandString, MySqlConnection);

		//	try
		//	{
		//		MySqlCommand.ExecuteNonQuery();
		//	}
		//	catch (Exception ex)
		//	{
		//		string res = ex.ToString();
		//		success = false;
		//	}
		//	finally
		//	{
		//		MySqlCommand.Dispose();
		//	}

		//	return success;

		//	//return (base.CreateNewDBEntry(data.UnitID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), KEY1_FIELD_NAME, KEY2_FIELD_NAME, XDUCER_TABLE_NAME));
		//}
	}
}
