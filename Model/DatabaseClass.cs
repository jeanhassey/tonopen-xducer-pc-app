﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace tonopen_xducer_pc_app.Model
{
	class DatabaseClass
	{
		protected MySqlConnection MySqlConnection { get; set; }

		public DatabaseClass()
		{
			MySqlConnection = new MySqlConnection();

			/*
			using (DbConnection connection = new SqlConnection("Your connection string"))
			{
				connection.Open();
				using (DbCommand command = new SqlCommand("alter table [Product] add [ProductId] int default 0 NOT NULL"))
				{
					command.Connection = connection;
					command.ExecuteNonQuery();
				}
			}*/
		}

		public bool OpenDatabase(string sServer, string sDbName, string sUsername, string sPassword)
		{
			bool Success = false;
			string Connection = string.Format("server={0}; user id={1}; password={2}; database={3}; pooling=false; SslMode=none", sServer, sUsername, sPassword, sDbName);

			try
			{
				if (MySqlConnection != null)
				{
					if (MySqlConnection.State != System.Data.ConnectionState.Open)
					{
						MySqlConnection.ConnectionString = Connection;
						MySqlConnection.Open();
						Success = true;
					}
				}
			}
			catch
			{
			}
			return Success;
		}

		public bool IsOpen()
		{
			return (MySqlConnection.State == System.Data.ConnectionState.Open);
		}

		public void CloseDatabase()
		{
			if (MySqlConnection != null)
			{
				if (MySqlConnection.State == System.Data.ConnectionState.Open)
					MySqlConnection.Close();
			}
		}

		public bool RecordExists(string sKey, string sKeyFieldName, string sTableName)
		{
			MySqlCommand MySqlCommand;
			MySqlDataReader MySqlReader;
			int count = 0;


			if (MySqlConnection.State == System.Data.ConnectionState.Open)
			{
				// Create string of the sql command that will be executed
				string sSelectString = "SELECT * FROM " + sTableName + " WHERE " + sKeyFieldName + " = '" + sKey + "'";

				MySqlCommand = new MySqlCommand(sSelectString, MySqlConnection);

				//try/catch
				MySqlReader = MySqlCommand.ExecuteReader();
				while (MySqlReader.Read())
				{
					count++;
				}
				MySqlReader.Close();
				MySqlCommand.Dispose();
			}

			return (count == 1);
		}

		public bool RecordExists(string sKey1, string sKey2, string sKey1FieldName, string sKey2FieldName, string sTableName)
		{
			MySqlCommand MySqlCommand;
			MySqlDataReader MySqlDataReader;
			int count = 0;


			if (MySqlConnection.State == System.Data.ConnectionState.Open)
			{
				// Create string of the sql command that will be executed
				string sSelectString = "SELECT * FROM " + sTableName + " WHERE " + sKey1FieldName + " = '" + sKey1 + "' AND " + sKey2FieldName + " = '" + sKey2 + "'";

				MySqlCommand = new MySqlCommand(sSelectString, MySqlConnection);

				MySqlDataReader = MySqlCommand.ExecuteReader();
				while (MySqlDataReader.Read())
				{
					count++;
				}
				MySqlDataReader.Close();
				MySqlCommand.Dispose();
			}

			return (count == 1);
		}

		public string ReadOneField(string sKey1, string sKeyFieldName1, string sTableName, string ColumnName)
		{
			string Result = String.Empty;

			if (MySqlConnection.State == System.Data.ConnectionState.Open)
			{
				// Create string of the sql command that will be executed
				string sSelectString = "SELECT " + ColumnName + " FROM " + sTableName + " WHERE " + sKeyFieldName1 + " = '" + sKey1 + "'";

				// Create an MySqlCommand object.
				MySqlCommand MySqlCommand = new MySqlCommand(sSelectString, MySqlConnection);

				try
				{
					MySqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();
					if (MySqlDataReader.Read())
					{
						Result = MySqlDataReader.GetValue(0).ToString();
					}
					MySqlDataReader.Close();
				}
				catch
				{

				}
				finally
				{
					MySqlCommand.Dispose();
				}
			}
			return Result;
		}
		public string ReadOneField(string sKey1, string sKey2, string sKeyFieldName1, string sKeyFieldName2, string sTableName, string ColumnName)
		{
			string Result = String.Empty;

			if (MySqlConnection.State == System.Data.ConnectionState.Open)
			{
				// Create string of the sql command that will be executed
				string SelectString = "SELECT " + ColumnName + " FROM " + sTableName + " WHERE " + sKeyFieldName1 + " = '" + sKey1 + "' AND " + sKeyFieldName2 + " = '" + sKey2 + "'";

				MySqlCommand MySqlCommand = new MySqlCommand(SelectString, MySqlConnection);
				try
				{
					MySqlDataReader MySqlDataReader = MySqlCommand.ExecuteReader();

					if (MySqlDataReader.Read())
					{
						Result = MySqlDataReader.GetValue(0).ToString();
					}
					MySqlDataReader.Close();
				}
				catch
				{
				}
				finally
				{
					MySqlCommand.Dispose();
				}
			}
			return Result;
		}

		//public string ReadMostRecent(string sKey1, string sKeyFieldName1, string sTableName, string sDateFieldName, string ColumnName)
		//{
		//	MySqlCommand MySqlCommand;
		//	MySqlDataReader MySqlDataReader;
		//	string Result = String.Empty;

		//	if (MySqlConnection.State == System.Data.ConnectionState.Open)
		//	{
		//		// Create string of the sql command that will be executed
		//		string SelectString = "SELECT " + ColumnName + " FROM " + sTableName + " WHERE " + sKeyFieldName1 + " = '" + sKey1 + "' ORDER BY " + sDateFieldName + " DESC LIMIT 1";

		//		// Create an MySqlCommand object.
		//		MySqlCommand = new MySqlCommand(SelectString, MySqlConnection);

		//		//        'Send the CommandText to the connection, and then build a MySqlDataReader.
		//		//        'Note: The MySqlDataReader is forward-only.
		//		try
		//		{
		//			MySqlDataReader = MySqlCommand.ExecuteReader();

		//			if (MySqlDataReader.Read())
		//			{
		//				Result = MySqlDataReader.GetValue(0).ToString();
		//			}
		//			MySqlDataReader.Close();
		//			MySqlCommand.Dispose();
		//		}
		//		catch
		//		{
		//		}
		//	}
		//	return Result;
		//}

		public bool ReadOneRow(string sKey1, string sKey2, string sKeyFieldName1, string sKeyFieldName2, string sTableName, ref string[] ColumnNames, ref string[] RowData)
		{
			MySqlCommand MySqlCommand;
			MySqlDataReader MySqlDataReader;
			bool bSuccess = false;

			if (MySqlConnection.State == System.Data.ConnectionState.Open)
			{
				// Create string of the sql command that will be executed
				string SelectString = "SELECT * FROM " + sTableName + " WHERE " + sKeyFieldName1 + " = '" + sKey1 + "' AND " + sKeyFieldName2 + " = '" + sKey2 + "'";

				// Create an MySqlCommand object.
				MySqlCommand = new MySqlCommand(SelectString, MySqlConnection);

				//        'Send the CommandText to the connection, and then build a MySqlDataReader.
				//        'Note: The MySqlDataReader is forward-only.
				MySqlDataReader = MySqlCommand.ExecuteReader();

				if (MySqlDataReader.Read())
				{
					bSuccess = true;

					RowData = new string[MySqlDataReader.FieldCount];
					ColumnNames = new string[MySqlDataReader.FieldCount];

					for (int i = 0; i < MySqlDataReader.FieldCount; i++)
					{
						ColumnNames[i] = MySqlDataReader.GetName(i);
						RowData[i] = MySqlDataReader.GetValue(i).ToString();
					}
				}
				MySqlDataReader.Close();
				MySqlCommand.Dispose();
			}
			return bSuccess;
		}

		public bool GetOneColumn(string sKeyFieldName, string sTableName, ref string[] sDatabaseField)
		{
			MySqlCommand MySqlCommand;
			MySqlDataReader MySqlDataReader;


			if (MySqlConnection.State == System.Data.ConnectionState.Open)
			{
				// Create string of the sql command that will be executed
				string SelectString = "SELECT " + sKeyFieldName + " FROM " + sTableName;

				// Create an MySqlCommand object.
				MySqlCommand = new MySqlCommand(SelectString, MySqlConnection);

				//        'Send the CommandText to the connection, and then build a MySqlDataReader.
				//        'Note: The MySqlDataReader is forward-only.
				MySqlDataReader = MySqlCommand.ExecuteReader();
				sDatabaseField = new string[5000];
				int i = 0;
				while (MySqlDataReader.Read())
				{
					//for (int i = 0; i < rMySqlReader.FieldCount; i++)
					sDatabaseField[i] = MySqlDataReader.GetValue(0).ToString();
					i++;
				}
				MySqlDataReader.Close();
				MySqlCommand.Dispose();
			}
			return true;
		}

		public bool CreateNewDBEntry(string sKey1, string sKey2, string sKey1FieldName, string sKey2FieldName, string sTableName)

		{
			//INSERT INTO Customer(FirstName, LastName, City, Country, Phone)
			//VALUES('Craig', 'Smith', 'New York', 'USA', 1 - 01 - 993 2800)
			bool success = true;

			string CommandString = "INSERT INTO " + sTableName + "(" + sKey1FieldName + ", " + sKey2FieldName + "," + "entry_type" + ") VALUES ('" + sKey1 + "', '" + sKey2 + "', '" + "0" + "')";

			MySqlCommand MySqlCommand = new MySqlCommand(CommandString, MySqlConnection);

			try
			{
				MySqlCommand.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				string res = ex.ToString();
				success = false;
			}
			finally
			{
				MySqlCommand.Dispose();
			}

			return success;
		}

		public bool RemoveDBEntry(string sKey1, string sKeyFieldName1, string sKey2, string sKeyFieldName2, string sTableName)
		{
			string sCommandString = "DELETE FROM " + sTableName + " WHERE " + sKeyFieldName1 + "='" + sKey1 + "' AND " + sKeyFieldName2 + "='" + sKey2 + "'";
			MySqlCommand MySqlCommand = new MySqlCommand(sCommandString, MySqlConnection);

			try
			{
				MySqlCommand.ExecuteNonQuery();
			}
			catch
			{
				return false;
			}
			finally
			{
				MySqlCommand.Dispose();
			}
			
			return true;
		}

		public bool WriteSQLDB(string sKey1, string sKey2, string sKey1FieldName, string sKey2FieldName, string sTableName, string sField, string sValue)
		{
			string CommandString = "UPDATE " + sTableName + " Set " + sField + " = '" + sValue + "' WHERE " + sKey1FieldName + " = '" + sKey1 + "' AND " + sKey2FieldName + " = '" + sKey2 + "'";
			MySqlCommand MySqlCommand = new MySqlCommand(CommandString, MySqlConnection);

			try
			{
				MySqlCommand.ExecuteNonQuery();
			}
			catch
			{
				return false;
			}
			finally
			{
				MySqlCommand.Dispose();
			}
	
			return true;
		}

		//public bool TestWriteSQLDB(string sKey1, string sKey1FieldName, string sTableName, string sField, string sValue)
		//{
		//	string CommandString = "UPDATE " + sTableName + " Set " + sField + " = '" + sValue + "' WHERE " + sKey1FieldName + " = '" + sKey1 + "'";
		//	MySqlCommand MySqlCommand = new MySqlCommand(CommandString, MySqlConnection);
			
		//	try
		//	{
		//		MySqlCommand.ExecuteNonQuery();
		//	}
		//	catch
		//	{
		//		return false;
		//	}
		//	finally
		//	{
		//		MySqlCommand.Dispose();
		//	}
			
		//	return true;
		//}
	}
}
