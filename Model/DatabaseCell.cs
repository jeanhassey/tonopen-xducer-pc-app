﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tonopen_xducer_pc_app.Model
{
	public enum DatabaseEntryType
	{
		DB_ENTRY_INIT,
		DB_ENTRY_UD,
		DB_ENTRY_UD_UNIT,
		DB_ENTRY_TEMPTEST,
		DB_ENTRY_TEMPCOMP,
		DB_ENTRY_PRECAL,
		DB_ENTRY_GAGE
	}

	public static class DatabaseRow
	{
		//public DatabaseRow(UnitData unitData)
		//{
		//}

		public static List<DatabaseCell> CreateTableEntry(DatabaseEntryType databaseEntryType, GageData gageData)
		{
			if (databaseEntryType == DatabaseEntryType.DB_ENTRY_GAGE)
			{
				List<DatabaseCell> RowData = CreateGageEntry(gageData);
				return RowData;
			}
			return null;
		}

		public static List<DatabaseCell> CreateTableEntry(DatabaseEntryType databaseEntryType, UnitData unitData, int testIndex)
		{
			if (databaseEntryType == DatabaseEntryType.DB_ENTRY_INIT)
			{
				List<DatabaseCell> KeyData = CreateKeyEntry(unitData, DatabaseEntryType.DB_ENTRY_INIT);
				List<DatabaseCell> RowData = CreateInitalRowEntry(unitData);
				KeyData.AddRange(RowData);

				return KeyData;
			}
			else if (databaseEntryType == DatabaseEntryType.DB_ENTRY_UD || databaseEntryType == DatabaseEntryType.DB_ENTRY_UD_UNIT)
			{
				//jmd testing only
				//jmdDataTest(unitData);
				// jmd end testing

				List<DatabaseCell> RowData = CreateUpDnTestEntry(unitData, testIndex, databaseEntryType);
				return RowData;
			}
			else if (databaseEntryType == DatabaseEntryType.DB_ENTRY_TEMPTEST)
			{
				List<DatabaseCell> KeyData = CreateKeyEntry(unitData, DatabaseEntryType.DB_ENTRY_TEMPTEST);
				List<DatabaseCell> RowData = CreateTemptestTestEntry(unitData, testIndex);
				KeyData.AddRange(RowData);
				return KeyData;
			}
			else if (databaseEntryType == DatabaseEntryType.DB_ENTRY_TEMPCOMP)
			{
				List<DatabaseCell> KeyData = CreateKeyEntry(unitData, DatabaseEntryType.DB_ENTRY_TEMPCOMP);
				List<DatabaseCell> RowData = CreateTempcompTestEntry(unitData, testIndex);
				KeyData.AddRange(RowData);
				return KeyData;
			}
			else if (databaseEntryType == DatabaseEntryType.DB_ENTRY_PRECAL)
			{
				List<DatabaseCell> RowData = CreatePrecalTestEntry(unitData);
				return RowData;
			}
		
			else return null;
		}

		private static List<DatabaseCell> CreateKeyEntry(UnitData unitData, DatabaseEntryType entryType)
		{
			List<DatabaseCell> RowEntry = new List<DatabaseCell>
			{
				new DatabaseCell("unit_id", typeof(string), unitData.UnitID),
				new DatabaseCell("record_datetime", typeof(DateTime), DateTime.Now),
				new DatabaseCell("model_type", typeof(int), unitData.Model),
				new DatabaseCell("entry_type", typeof(int), entryType),
				new DatabaseCell("file_source_type", typeof(int), unitData.FileType),
				new DatabaseCell("work_order", typeof(string), unitData.WorkOrder)
			};

			return RowEntry;
		}

		private static List<DatabaseCell> CreateGageEntry(GageData gageData)
		{
			List<DatabaseCell> RowEntry = new List<DatabaseCell>
			{
				new DatabaseCell("unit_id", typeof(string), gageData.UnitID),
				new DatabaseCell("record_datetime", typeof(DateTime), DateTime.Now),
				new DatabaseCell("model_type", typeof(int), gageData.Model),
				new DatabaseCell("file_source_type", typeof(int), gageData.Model),
				new DatabaseCell("entry_type", typeof(int), DatabaseEntryType.DB_ENTRY_INIT), //this will go on inital entry of this key
				new DatabaseCell("work_order", typeof(string), gageData.WorkOrder),
				new DatabaseCell("gage_date", typeof(DateTime), gageData.Date),
				new DatabaseCell("gager_id", typeof(string), gageData.Initials),
				new DatabaseCell("bond_1", typeof(int), gageData.Bond1),
				new DatabaseCell("bond_2", typeof(int), gageData.Bond2),
				new DatabaseCell("bond_3", typeof(int), gageData.Bond3),
				new DatabaseCell("bond_4", typeof(int), gageData.Bond4),
				new DatabaseCell("unbond_1", typeof(int), gageData.Unbond1),
				new DatabaseCell("unbond_2", typeof(int), gageData.Unbond2),
				new DatabaseCell("unbond_3", typeof(int), gageData.Unbond3),
				new DatabaseCell("unbond_4", typeof(int), gageData.Unbond4)
			};

			return RowEntry;
		}

		private static List<DatabaseCell> CreatePrecalTestEntry(UnitData unitData)
		{
			if (unitData.TempCompTest.Count != 1 || unitData.PrecalTest.Count != 3)
			{
				return null;
			}
			else if (unitData.TempCompTest[0].Data.Count != 4)
				return null;

			List<DatabaseCell> RowEntry = new List<DatabaseCell>();

			RowEntry.Add(new DatabaseCell("unit_id", typeof(string), unitData.UnitID));
			RowEntry.Add(new DatabaseCell("model_type", typeof(int), unitData.Model));
			RowEntry.Add(new DatabaseCell("record_datetime", typeof(DateTime), DateTime.Now));
			RowEntry.Add(new DatabaseCell("work_order", typeof(string), unitData.WorkOrder));
			RowEntry.Add(new DatabaseCell("tempcomp_datetime", typeof(DateTime), unitData.TempCompTest[0].Data[0].DateTime));

			RowEntry.Add(new DatabaseCell("tempcomp_temp", typeof(double), unitData.TempCompTest[0].Temperature));

			for(int i = 0;i < 4;i++)
			{
				RowEntry.Add(new DatabaseCell("tempcomp_r" + unitData.TempCompTest[0].Data[i].RVal.ToString(), typeof(double), unitData.TempCompTest[0].Data[i].Data));
			}

			for(int i = 0;i < 3;i++)
			{
				RowEntry.Add(new DatabaseCell("precal_datetime" + (i+1).ToString(), typeof(DateTime), unitData.PrecalTest[i].PrecalData[0].DateTime));
				RowEntry.Add(new DatabaseCell("precal_temp" + (i+1).ToString(), typeof(double), unitData.PrecalTest[i].Temperature));
				RowEntry.Add(new DatabaseCell("precal_val" + (i+1).ToString(), typeof(double), unitData.PrecalTest[i].PrecalData[0].Data));
			}
			return RowEntry;
		}

		private static List<DatabaseCell> CreateTemptestTestEntry(UnitData unitData, int testIndex)
		{
			//List<DatabaseCell> RowEntry = CreateKeyEntry(unitData);

			List<DatabaseCell> RowEntry = new List<DatabaseCell>
			{
				//new DatabaseCell("entry_type", typeof(int), DatabaseEntryType.DB_ENTRY_TEMPTEST),

				new DatabaseCell("temptest_datetime", typeof(DateTime), unitData.TempTest[testIndex].DateTime),
				new DatabaseCell("temptest_temp", typeof(double), unitData.TempTest[testIndex].Temperature),
				new DatabaseCell("temptest_excitation", typeof(double), unitData.TempTest[testIndex].Excitation),
				new DatabaseCell("temptest_data", typeof(double), unitData.TempTest[testIndex].Data)
			};

			//RowEntry.AddRange(TestEntry);
			return RowEntry;
		}

		private static List<DatabaseCell> CreateTempcompTestEntry(UnitData unitData, int testIndex)
		{
			//List<DatabaseCell> RowEntry = CreateKeyEntry(unitData);

			List<DatabaseCell> RowEntry = new List<DatabaseCell>
			{
				//new DatabaseCell("entry_type", typeof(int), DatabaseEntryType.DB_ENTRY_TEMPCOMP),

				new DatabaseCell("tempcomp_datetime", typeof(DateTime), unitData.TempCompTest[testIndex].Data[0].DateTime),
				new DatabaseCell("tempcomp_temp", typeof(double), unitData.TempCompTest[testIndex].Temperature),
				new DatabaseCell("tempcomp_r1", typeof(double), unitData.TempCompTest[testIndex].Data[0].Data),
				new DatabaseCell("tempcomp_r2", typeof(double), unitData.TempCompTest[testIndex].Data[1].Data),
				new DatabaseCell("tempcomp_r3", typeof(double), unitData.TempCompTest[testIndex].Data[2].Data),
				new DatabaseCell("tempcomp_r4", typeof(double), unitData.TempCompTest[testIndex].Data[3].Data)
			};

			//RowEntry.AddRange(TestEntry);
			return RowEntry;
		}

		//private static List<DatabaseCell> CreateUpDnUnitTestEntry(UnitData unitData, int testIndex)
		//{
		//	//List<DatabaseCell> RowEntry = CreateKeyEntry(unitData);

		//	List<DatabaseCell> RowEntry = new List<DatabaseCell>
		//	{
		//		//new DatabaseCell("entry_type", typeof(int), DatabaseEntryType.DB_ENTRY_UD_UNIT),
				
		//		//new DatabaseCell("udunit_datetime1", typeof(DateTime), unitData.UpDownUnit[testIndex].UpDownData[0].DateTime),
		//		//new DatabaseCell("udunit_temp1", typeof(double), unitData.UpDownUnit[testIndex].Temperature),
		//		//new DatabaseCell("udunit_data1", typeof(double), unitData.UpDownUnit[testIndex].UpDownData[0].Data),
		//		//new DatabaseCell("udunit_datetime2", typeof(DateTime), unitData.UpDownUnit[testIndex].UpDownData[1].DateTime),
		//		//new DatabaseCell("udunit_temp2", typeof(double), unitData.UpDownUnit[testIndex].Temperature),
		//		//new DatabaseCell("udunit_data2", typeof(double), unitData.UpDownUnit[testIndex].UpDownData[1].Data),
		//		//new DatabaseCell("udunit_datetime3", typeof(DateTime), unitData.UpDownUnit[testIndex].UpDownData[2].DateTime),
		//		//new DatabaseCell("udunit_temp3", typeof(double), unitData.UpDownUnit[testIndex].Temperature),
		//		//new DatabaseCell("udunit_data3", typeof(double), unitData.UpDownUnit[testIndex].UpDownData[2].Data),

		//		new DatabaseCell("unit_id", typeof(string), unitData.UnitID),
		//		new DatabaseCell("work_order", typeof(string), unitData.WorkOrder),
		//		new DatabaseCell("record_datetime", typeof(DateTime), DateTime.Now),
		//		new DatabaseCell("source_id", typeof(int), unitData.FileType),
		//		new DatabaseCell("blocktype_id", typeof(int), BlockType.BLOCK_TYPE_UPDOWNUNIT),
		//		new DatabaseCell("updown_temp", typeof(double), unitData.UpDownTest[testIndex].Temperature),
		//		new DatabaseCell("ud_excitation", typeof(double), unitData.UpDownTest[testIndex].Excitation),

		//		new DatabaseCell("ud_datetime1", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[0].DateTime),
		//		new DatabaseCell("ud_val1", typeof(double), unitData.UpDownTest[testIndex].UpDownData[0].Data),

		//		new DatabaseCell("ud_datetime2", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[1].DateTime),
		//		new DatabaseCell("ud_val2", typeof(double), unitData.UpDownTest[testIndex].UpDownData[1].Data),

		//		new DatabaseCell("ud_datetime3", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[2].DateTime),
		//		new DatabaseCell("ud_val3", typeof(double), unitData.UpDownTest[testIndex].UpDownData[2].Data),
		//	};

		//	//RowEntry.AddRange(TestEntry);
		//	return RowEntry;
		//}

		private static List<DatabaseCell> CreateUpDnTestEntry(UnitData unitData, int testIndex, DatabaseEntryType databaseEntryType)
		{
			List<DatabaseCell> RowEntry = new List<DatabaseCell>
			{
				//new DatabaseCell("entry_type", typeof(int), DatabaseEntryType.DB_ENTRY_UD),
				new DatabaseCell("unit_id", typeof(string), unitData.UnitID),
				new DatabaseCell("work_order", typeof(string), unitData.WorkOrder),
				new DatabaseCell("record_datetime", typeof(DateTime), DateTime.Now),
				new DatabaseCell("source_id", typeof(int), unitData.FileType),
				new DatabaseCell("blocktype_id", typeof(int), databaseEntryType),
				new DatabaseCell("updown_temp", typeof(double), unitData.UpDownTest[testIndex].Temperature), 
				new DatabaseCell("ud_excitation", typeof(double), unitData.UpDownTest[testIndex].Excitation), 

				new DatabaseCell("ud_datetime1", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[0].DateTime),
				new DatabaseCell("ud_val1", typeof(double), unitData.UpDownTest[testIndex].UpDownData[0].Data),

				new DatabaseCell("ud_datetime2", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[1].DateTime),
				new DatabaseCell("ud_val2", typeof(double), unitData.UpDownTest[testIndex].UpDownData[1].Data),

				new DatabaseCell("ud_datetime3", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[2].DateTime),
				new DatabaseCell("ud_val3", typeof(double), unitData.UpDownTest[testIndex].UpDownData[2].Data)

				//,
				//,

				//new DatabaseCell("ud_datetime5", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[4].DateTime),
				//new DatabaseCell("ud_val5", typeof(double), unitData.UpDownTest[testIndex].UpDownData[4].Data),

				//new DatabaseCell("ud_datetime6", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[5].DateTime),
				//new DatabaseCell("ud_val6", typeof(double), unitData.UpDownTest[testIndex].UpDownData[5].Data),
			};

			if(databaseEntryType == DatabaseEntryType.DB_ENTRY_UD)
			{
				RowEntry.Add(new DatabaseCell("ud_datetime4", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[3].DateTime));
				RowEntry.Add(new DatabaseCell("ud_val4", typeof(double), unitData.UpDownTest[testIndex].UpDownData[3].Data));

				RowEntry.Add(new DatabaseCell("ud_datetime5", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[4].DateTime));
				RowEntry.Add(new DatabaseCell("ud_val5", typeof(double), unitData.UpDownTest[testIndex].UpDownData[4].Data));

				RowEntry.Add(new DatabaseCell("ud_datetime6", typeof(DateTime), unitData.UpDownTest[testIndex].UpDownData[5].DateTime));
				RowEntry.Add(new DatabaseCell("ud_val6", typeof(double), unitData.UpDownTest[testIndex].UpDownData[5].Data));
			}

			return RowEntry;
		}

		private static List<DatabaseCell> CreateInitalRowEntry(UnitData unitData)
		{
			List<DatabaseCell> RowEntry = new List<DatabaseCell>();

			//RowEntry.Add(new DatabaseCell("entry_type", typeof(int), DatabaseEntryType.DB_ENTRY_INIT));

			// Resonance
			RowEntry.Add(new DatabaseCell("peak_freq_hz", typeof(double), null));
			RowEntry.Add(new DatabaseCell("width_hz", typeof(double), null));

			// 
			//RowEntry.Add(new DatabaseCell("init_r_datetime", typeof(DateTime), null));
			//RowEntry.Add(new DatabaseCell("init_r1", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("init_r2", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("init_r3", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("init_r4", typeof(double), null));

			//RowEntry.Add(new DatabaseCell("before_t_datetime", typeof(DateTime), null));
			//RowEntry.Add(new DatabaseCell("before_t_temp", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("before_t_volts", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("after_t_datetime", typeof(DateTime), null));
			//RowEntry.Add(new DatabaseCell("after_t_temp", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("after_t_volts", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("after_bi_datetime", typeof(DateTime), null));
			//RowEntry.Add(new DatabaseCell("after_bi_temp", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("after_bi_volts", typeof(double), null));

			RowEntry.Add(new DatabaseCell("gage_date", typeof(DateTime), null));
			RowEntry.Add(new DatabaseCell("gager_id", typeof(string), null));
			RowEntry.Add(new DatabaseCell("bond_1", typeof(int), null));
			RowEntry.Add(new DatabaseCell("bond_2", typeof(int), null));
			RowEntry.Add(new DatabaseCell("bond_3", typeof(int), null));
			RowEntry.Add(new DatabaseCell("bond_4", typeof(int), null));
			RowEntry.Add(new DatabaseCell("unbond_1", typeof(int), null));
			RowEntry.Add(new DatabaseCell("unbond_2", typeof(int), null));
			RowEntry.Add(new DatabaseCell("unbond_3", typeof(int), null));
			RowEntry.Add(new DatabaseCell("unbond_4", typeof(int), null));

			//if (unitData.UpDownUnit.Count > 0)
			//{
			//	List<DatabaseCell> UnitUDEntry = CreateUpDnUnitTestEntry(unitData, 0);
			//	RowEntry.AddRange(UnitUDEntry);
			//}
			// UnitU/D 
			//RowEntry.Add(new DatabaseCell("udunit_datetime1", typeof(DateTime), null));
			//RowEntry.Add(new DatabaseCell("udunit_temp1", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("udunit_data1", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("udunit_datetime2", typeof(DateTime), null));
			//RowEntry.Add(new DatabaseCell("udunit_temp2", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("udunit_data2",  typeof(double), null));
			//RowEntry.Add(new DatabaseCell("udunit_datetime3",  typeof(DateTime), null));
			//RowEntry.Add(new DatabaseCell("udunit_temp3", typeof(double), null));
			//RowEntry.Add(new DatabaseCell("udunit_data3", typeof(double), null));

			RowEntry.Add(new DatabaseCell("real_r_val1", typeof(double), null));
			RowEntry.Add(new DatabaseCell("real_r_val2", typeof(double), null));
			RowEntry.Add(new DatabaseCell("real_r_val3", typeof(double), null));
			RowEntry.Add(new DatabaseCell("real_r_val4", typeof(double), null));
			RowEntry.Add(new DatabaseCell("real_r_val5", typeof(double), null));
			RowEntry.Add(new DatabaseCell("real_r_val6", typeof(double), null));

			RowEntry.Add(new DatabaseCell("ud_span_target", typeof(double), null));

			if (unitData.SpanTest.Count > 0)
			{
				RowEntry.Add(new DatabaseCell("force_datetime", typeof(DateTime), unitData.SpanTest[0].DateTime0g));
				RowEntry.Add(new DatabaseCell("force_excitation", typeof(double), unitData.SpanTest[0].Excitation));
				RowEntry.Add(new DatabaseCell("force_temp", typeof(double), unitData.SpanTest[0].Temperature));
				RowEntry.Add(new DatabaseCell("force_test_0g", typeof(double), unitData.SpanTest[0].Data0g));
				RowEntry.Add(new DatabaseCell("force_test_3g", typeof(double), unitData.SpanTest[0].Data3g));
				
			}

			

			FinalTestData finalTestData = null;

			if (unitData.Model == ModelType.MODEL_TYPE_XL && unitData.SpanTest.Count == 1 && unitData.TempTest.Count >=4 && unitData.UpDownTest.Count == 1)
			{
				FinalTestDataP033 finalTestDataP033 = new FinalTestDataP033(unitData);
				RowEntry.Add(new DatabaseCell("final_updown_delta", typeof(double), finalTestDataP033.TempTestLo));
				RowEntry.Add(new DatabaseCell("final_temp_zero15", typeof(double), finalTestDataP033.TempTestMid));
				RowEntry.Add(new DatabaseCell("final_temp_zero35", typeof(double), finalTestDataP033.TempTestHi));
				finalTestData = finalTestDataP033;
			}
			else if (unitData.Model == ModelType.MODEL_TYPE_AVIA && unitData.SpanTest.Count == 1 && unitData.UpDownTest.Count > 3)
			{
				FinalTestDataP048 finalTestDataP048 = new FinalTestDataP048(unitData);
				RowEntry.Add(new DatabaseCell("final_updown_delta", typeof(double), finalTestDataP048.UpDownDelta));
				RowEntry.Add(new DatabaseCell("final_temp_zero15", typeof(double), finalTestDataP048.TempTestZero15));
				RowEntry.Add(new DatabaseCell("final_temp_zero35", typeof(double), finalTestDataP048.TempTestZero35));
				RowEntry.Add(new DatabaseCell("final_temp_sens15", typeof(double), finalTestDataP048.TempTestSens15));
				RowEntry.Add(new DatabaseCell("final_temp_sens35", typeof(double), finalTestDataP048.TempTestSens35));
				finalTestData = finalTestDataP048;
			}

			if(finalTestData != null)
			{
				RowEntry.Add(new DatabaseCell("final_datetime", typeof(DateTime), finalTestData.Date));
				RowEntry.Add(new DatabaseCell("final_excitation", typeof(double), finalTestData.Excitation));
				RowEntry.Add(new DatabaseCell("force_test_span", typeof(double), finalTestData.Span));
			}
			// UpDnTest
			//if (unitData.UpDownTest.Count > 0)
			//{
			//	List<DatabaseCell> UDEntry = CreateUpDnTestEntry(unitData, 0);
			//	RowEntry.AddRange(UDEntry);
			//}
			//RowEntry.Add(new DatabaseCell("final_ud_datetime1", typeof(DateTime), unitData.UpDownTest[0].UpDownData[0].DateTime));
			//RowEntry.Add(new DatabaseCell("final_ud_temp1", typeof(double), unitData.UpDownTest[0].Temperature));
			//RowEntry.Add(new DatabaseCell("final_ud_1", typeof(double), unitData.UpDownTest[0].UpDownData[0].Data));
			//RowEntry.Add(new DatabaseCell("final_ud_datetime2", typeof(DateTime), unitData.UpDownTest[0].UpDownData[1].DateTime));
			//RowEntry.Add(new DatabaseCell("final_ud_temp2", typeof(double), unitData.UpDownTest[0].Temperature));
			//RowEntry.Add(new DatabaseCell("final_ud_2", typeof(double), unitData.UpDownTest[0].UpDownData[1].Data));
			//RowEntry.Add(new DatabaseCell("final_ud_datetime3", typeof(DateTime), unitData.UpDownTest[0].UpDownData[2].DateTime));
			//RowEntry.Add(new DatabaseCell("final_ud_temp3", typeof(double), unitData.UpDownTest[0].Temperature));
			//RowEntry.Add(new DatabaseCell("final_ud_3", typeof(double), unitData.UpDownTest[0].UpDownData[2].Data));
			//RowEntry.Add(new DatabaseCell("final_ud_datetime4", typeof(DateTime), unitData.UpDownTest[0].UpDownData[3].DateTime));
			//RowEntry.Add(new DatabaseCell("final_ud_temp4", typeof(double), unitData.UpDownTest[0].Temperature));
			//RowEntry.Add(new DatabaseCell("final_ud_4", typeof(double), unitData.UpDownTest[0].UpDownData[3].Data));
			//RowEntry.Add(new DatabaseCell("final_ud_datetime5", typeof(DateTime), unitData.UpDownTest[0].UpDownData[4].DateTime));
			//RowEntry.Add(new DatabaseCell("final_ud_temp5", typeof(double), unitData.UpDownTest[0].Temperature));
			//RowEntry.Add(new DatabaseCell("final_ud_5", typeof(double), unitData.UpDownTest[0].UpDownData[4].Data));
			//RowEntry.Add(new DatabaseCell("final_ud_datetime6", typeof(DateTime), unitData.UpDownTest[0].UpDownData[5].DateTime));
			//RowEntry.Add(new DatabaseCell("final_ud_temp6", typeof(double), unitData.UpDownTest[0].Temperature));
			//RowEntry.Add(new DatabaseCell("final_ud_6", typeof(double), unitData.UpDownTest[0].UpDownData[5].Data));

			//WarmUp
			if (unitData.WarmUpTest.Count > 0)
			{
				RowEntry.Add(new DatabaseCell("warmup_datetime", typeof(DateTime), unitData.WarmUpTest[0].Date));
				RowEntry.Add(new DatabaseCell("warmup_excitation", typeof(double), unitData.WarmUpTest[0].Excitation));

				for(int i = 1; i <= 20;i++)
				{
					RowEntry.Add(new DatabaseCell("warmup_" + i.ToString() + "sec", typeof(double), unitData.WarmUpTest[0].WarmUpData[i-1].Data));
				}

				RowEntry.Add(new DatabaseCell("warmup_delta_e", typeof(double), unitData.WarmUpTest[0].DeltaE));
			}

			//Rin/Rout jmd assume one test?
			if (unitData.ResInOut.Count > 0)
			{ 
				RowEntry.Add(new DatabaseCell("resistance_io_temp", typeof(double), unitData.ResInOut[0].Temperature));
				RowEntry.Add(new DatabaseCell("resistance_io_datetimeout", typeof(DateTime), unitData.ResInOut[0].DateTimeOut));
				RowEntry.Add(new DatabaseCell("resistance_io_out", typeof(double), unitData.ResInOut[0].DataOut));
				RowEntry.Add(new DatabaseCell("resistance_io_datetimein", typeof(DateTime), unitData.ResInOut[0].DateTimeIn));
				RowEntry.Add(new DatabaseCell("resistance_io_in", typeof(double), unitData.ResInOut[0].DataIn));
			}

			// IOPTest jmd assume one test?
			if (unitData.IOPTest.Count > 0)
			{
				RowEntry.Add(new DatabaseCell("iop_pressure", typeof(int), unitData.IOPTest[0].Pressure));

				for(int i = 0;i < 3;i++)
				{
					RowEntry.Add(new DatabaseCell("iop_datetime" + (i+1).ToString(), typeof(DateTime), unitData.IOPTest[0].IOPData[i].DateTime));
					RowEntry.Add(new DatabaseCell("iop_press" + (i + 1).ToString(), typeof(int), unitData.IOPTest[0].IOPData[i].Pressure));
					RowEntry.Add(new DatabaseCell("iop_data" + (i + 1).ToString(), typeof(int), unitData.IOPTest[0].IOPData[i].Data));
				}
			}

			if (unitData.TempTest.Count > 0)
			{
				List<DatabaseCell> TemptestEntry = CreateTemptestTestEntry(unitData, 0);
				RowEntry.AddRange(TemptestEntry);
			}

			if (unitData.TempCompTest.Count > 0)
			{
				List<DatabaseCell> TempcompEntry = CreateTempcompTestEntry(unitData, 0);
				RowEntry.AddRange(TempcompEntry);
			}

			return RowEntry;
		}
	}

	public class DatabaseCell
	{
		public DatabaseCell(string cn, Type t, object d)
		{
			ColumnName = cn;
			DataType = t;
			Data = d;
			DataString = string.Empty;

			if (Data != null)
			{
				if (DataType == Type.GetType("System.Int32"))
				{
					DataString = ((int)Data).ToString();
				}
				else if (DataType == Type.GetType("System.Double"))
				{
					DataString = ((double)Data).ToString();
				}
				else if (DataType == Type.GetType("System.DateTime"))
				{
					DataString = ((DateTime)Data).ToString("yyyy-MM-dd HH:mm:ss.fff");
				}
				else
				{
					DataString = (string)Data;
				}
			}
		}
		public string ColumnName { get; set; }
		public Type DataType { get; set; }
		public string DataString { get; set; }
		public object Data { get; set; }
	}

	//public class DatabaseCellDateTime : DatabaseCellBase
	//{
	//	public DateTime Data { get; set; }
	//}

	//public class DatabaseCellDateDouble : DatabaseCellBase
	//{
	//	private double _data;
	//	public double Data
	//	{
	//		get { return _data; }
	//		set { _data = value; DataString = _data.ToString(); }
	//	}
	//}

	//public class DatabaseCellDateInt : DatabaseCellBase
	//{
	//	private int _data;
	//	public int Data
	//	{
	//		get { return _data; }
	//		set { _data = value; DataString = _data.ToString(); }
	//	}
	//}
}
